#!/bin/bash

# create_masterfiles.sh
#
#
# Created by Cristiana Tisca on 16/03/2022


if [ "$1" == -h ]
then

echo "create_masterfile.sh -no_reps <number_of_repetitions> -no_channels <number_of_channels> -voxelsize <resolution_in_mm> \
-no_TEs <number_of_echo_times> -first_TE <first_echo_time> -IES <inter_echo_spacing -output_folder <output_folder>"

exit
fi

while [ ! -z "$1" ]
do
case "$1" in
-no_reps) no_reps=${2};shift;;
-no_channels) no_channels=${2};shift;;
-voxelsize) voxelsize=${2};shift;;
-no_TEs) no_TEs=${2};shift;;
-first_TE) first_TE=${2};shift;;
-IES) IES=${2};shift;;
-sample_name) sample_name=${2};shift;;
-output_folder) output_folder=${2};shift;;
esac
#Prevent error using shift over 0 inputs
if [ $# -gt 0 ]
then
shift
fi

done

mkdir $output_folder/masterfiles
echo "$no_reps" >> $output_folder/masterfiles/no_reps
echo "$no_channels" >> $output_folder/masterfiles/no_channels
echo "$voxelsize" >> $output_folder/masterfiles/voxelsize
echo "$no_TEs" >> $output_folder/masterfiles/no_TEs
echo "$first_TE" >> $output_folder/masterfiles/first_TE
echo "$IES" >> $output_folder/masterfiles/IES
echo "$sample_name" >> $output_folder/masterfiles/sample_name
