if [ $# -lt 3]
then
    echo "Usage: $0 inputdir sample_name brain_mask"
    echo "This script takes the output directory from the QSM/R2s pipeline"
    echo "and the brain mask and outputs a susceptibility map where the values"
    echo "are referenced to the mean across the mask."
    echo "This is the mask which should be used for higher-order analysis."
    echo "Author: Cristiana Tisca."

    exit 0
fi

inputdir=$1
sample_name=$2
brain_mask=$3

fslmaths ${inputdir}/${sample_name}/${sample_name}_qsm/${sample_name}_susceptibility.nii.gz -sub \
`fslstats ${inputdir}/${sample_name}/${sample_name}_qsm/${sample_name}_susceptibility.nii.gz -k \
${brain_mask} -m` -mas ${brain_mask} \
${inputdir}/${sample_name}/${sample_name}_qsm/${sample_name}_susceptibility_mask_referenced.nii.gz
