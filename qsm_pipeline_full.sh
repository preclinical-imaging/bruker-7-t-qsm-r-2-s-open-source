#!/bin/bash

# qsm_pipeline_full.sh
#
#
# Created by Cristiana Tisca on 16/04/2021
# Updated on 15/01/2022

module add mrdegibbs3d

if [ "$1" == -h ]
then

echo "qsm_pipeline_full -ifmag1 <input_folder_mag_nifti_1> -ifmag2 <input_folder_2> -ifcomplex1 <input_folder_complex_2dseq_1> \
-ifcomplex2 <input_folder_complex_2dseq_2> -mask <atlas or intensity-based thresholded brain mask> -sn <sample_name> -o <output_folder>"

exit
fi

while [ ! -z "$1" ]
do
case "$1" in
-ifmag1) ifmag1=${2};shift;;
-ifmag2) ifmag2=${2};shift;;
-ifcomplex1) ifcomplex1=${2};shift;;
-ifcomplex2) ifcomplex2=${2};shift;;
-mask) mask=${2};shift;;
-sn) sn=${2};shift;;
-o) o=${2};shift;;
esac
#Prevent error using shift over 0 inputs
if [ $# -gt 0 ]
then
shift
fi

done


mkdir ${o}/${sn} ${o}/${sn}/${sn}_reg ${o}/${sn}/${sn}_complex_data ${o}/${sn}/${sn}_qsm ${o}/${sn}/${sn}_t2s ${o}/${sn}/logs

echo running magnitude data registration
jid1=`fsl_sub -q short.q -l ${o}/${sn}/logs bruker-7-t-qsm-r-2-s-open-source/reg_magnitude_correct_orient.sh \
 ${ifmag1} ${ifmag2} ${b0ge} ${sn} ${o}/${sn}/${sn}_reg`
echo $jid1

echo queuing reading 2dseq complex data
jid2=`fsl_sub -q veryshort.q -j $jid1 -l ${o}/${sn}/logs -s openmp,4 \
matlab -singleCompThread -nodisplay -nosplash -r \
"addpath('bruker-7-t-qsm-r-2-s-open-source/QSMprocessing');get_real_imag_bigFOV('${ifcomplex1}', '${ifcomplex2}', '${o}/${sn}/${sn}_complex_data')"`
echo $jid2

echo queueing complex data registration
jid3=`fsl_sub -q short.q -j $jid2 -l ${o}/${sn}/logs bruker-7-t-qsm-r-2-s-open-source/register_complex_correct_orient.sh ${o} ${sn}`
echo $jid3

echo 3D gibbs correction
jid4=`fsl_sub -q short.q -j $jid3 -l ${o}/${sn}/${sn}_reg/gibbslogs deGibbs3D ${o}/${sn}/${sn}_reg/${sn}_rep1.nii.gz ${o}/${sn}/${sn}_reg/${sn}_rep1_gibbs.nii.gz`
jid5=`fsl_sub -q short.q -j $jid4 -l ${o}/${sn}/${sn}_reg/gibbslogs deGibbs3D ${o}/${sn}/${sn}_reg/${sn}_rep2.nii.gz ${o}/${sn}/${sn}_reg/${sn}_rep2_gibbs.nii.gz`
jid6=`fsl_sub -q short.q -j $jid5 -l ${o}/${sn}/${sn}_reg/gibbslogs deGibbs3D ${o}/${sn}/${sn}_reg/${sn}_rep12avg.nii.gz ${o}/${sn}/${sn}_reg/${sn}_rep12_avg_gibbs.nii.gz`
echo $jid4 $jid5 $jid6

echo queuing coil combination and phase offset removal
jid7=`fsl_sub -q veryshort.q -j $jid6 \
-l ${o}/${sn}/logs -s openmp,4 matlab -singleCompThread -nodisplay \
-nosplash -r " addpath('bruker-7-t-qsm-r-2-s-open-source/QSMprocessing');qsm_step1_reg('${o}/${sn}/','${sn}')"`
echo $jid7

echo queuing field map estimation
jid8=`bash bruker-7-t-qsm-r-2-s-open-source/QSMprocessing/QSM_mouse_2.sh \
-o  ${o}/${sn}/post_processing_${sn}_rep1 -r 0.1 -j $jid7`
jid9=`bash bruker-7-t-qsm-r-2-s-open-source/QSMprocessing/QSM_mouse_2.sh \
-o  ${o}/${sn}/post_processing_${sn}_rep2 -r 0.1 -j $jid8`
echo $jid8 $jid9

echo queuing background field removal and dipole inversion each rep separately
jid10=`fsl_sub -q veryshort.q -j $jid9 \
-l ${o}/${sn}/logs -s openmp,4 matlab -singleCompThread -nodisplay -nosplash \
-r "addpath('bruker-7-t-qsm-r-2-s-open-source/QSMprocessing');qsm_step2_onerep_rP0f('${o}/${sn}/post_processing_${sn}_rep1', \
'${mask}', '${o}/${sn}/${sn}_qsm/', '${sn}',  '_rep1')"`
echo $jid10
jid11=`fsl_sub -q veryshort.q -j $jid10 \
-l ${o}/${sn}/logs -s openmp,4 matlab -singleCompThread -nodisplay -nosplash \
-r "addpath('bruker-7-t-qsm-r-2-s-open-source/QSMprocessing');qsm_step2_onerep_rP0f('${o}/${sn}/post_processing_${sn}_rep2', \
'${mask}', '${o}/${sn}/${sn}_qsm/', '${sn}',  '_rep2')"`
echo $jid11

echo queuing background field removal and dipole inversion - reps combined
jid12=`fsl_sub -q veryshort.q -j $jid11 \
-l ${o}/${sn}/logs -s openmp,4 matlab -singleCompThread -nodisplay -nosplash \
-r "addpath('bruker-7-t-qsm-r-2-s-open-source/QSMprocessing');qsm_step2_tworep_rP0f('${o}/${sn}/post_processing_${sn}_rep1', \
'${o}/${sn}/post_processing_${sn}_rep2', '${mask}', '${o}/${sn}/${sn}_qsm/', '${sn}',  '_combined_reps')"`
echo $jid12

echo queuing T2s pipeline each rep separately
jid13=`bash bruker-7-t-qsm-r-2-s-open-source/QSMprocessing/T2s_pipeline.sh -j $jid12 -o ${o}/${sn}/post_processing_${sn}_rep1 -r 0.1 -i \
${o}/${sn}/${sn}_reg/${sn}_rep1_gibbs.nii.gz -sn ${sn} -lf ${o}/${sn}/ -repno rep1 -mask ${mask}`
jid14=`bash bruker-7-t-qsm-r-2-s-open-source/QSMprocessing/T2s_pipeline.sh -j $jid13 -o ${o}/${sn}/post_processing_${sn}_rep2 -r 0.1 -i \
${o}/${sn}/${sn}_reg/${sn}_rep2_gibbs.nii.gz -sn ${sn} -lf ${o}/${sn}/ -repno rep2 -mask ${mask}`
echo $jid13

echo queuing T2s pipeline combined reps
jid15=`bash bruker-7-t-qsm-r-2-s-open-source/QSMprocessing/T2s_pipeline.sh -j $jid14 -o ${o}/${sn}/post_processing_${sn}_t2s_reps_combined \
-r 0.1 -i ${o}/${sn}/${sn}_reg/${sn}_rep12_avg_gibbs.nii.gz -sn ${sn} -lf ${o}/${sn}/ -repno combined_reps -mask ${mask}`

echo queuing fileprep
jid16=`fsl_sub -q veryshort.q -j $jid15 -l ${o}/${sn}/logs bruker-7-t-qsm-r-2-s-open-source/qsm_pipeline_headers.sh -o ${o} -sn ${sn}`
echo $jid16
