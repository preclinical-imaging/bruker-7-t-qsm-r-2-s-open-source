#!/bin/bash

if [ "$1" == -h ]
then

echo "reg_complex_correct_orient_gen_v2 -o <output_file> -sn <sample_name> -no_channels <number_of_channels> -no_reps <number_of_repetitions>"
exit
fi

while [ ! -z "$1" ]
do
case "$1" in
-o) o=${2};shift;;
-sn) sn=${2};shift;;
-no_channels) no_channels=${2};shift;;
-no_reps) no_reps=${2};shift;;
-no_TEs) no_TEs=${2};shift;;
esac
#Prevent error using shift over 0 inputs
if [ $# -gt 0 ]
then
shift
fi
done

orient_corr () {
fslorient -deleteorient $1
fslswapdim $1 z -y x $1
fslorient -setsform 0.1 0 0 0 0 0.1 0 0 0 0 0.1 0 0 0 0 1 $1
fslorient -copysform2qform $1
fslorient -setsformcode 1 $1
fslorient -setqformcode 1 $1
}

LAST_ECHO=`expr ${no_TEs} - 1`

for repno in `seq 1 $no_reps`
do
for complex in real imag
do
for chno in `seq 1 $no_channels`
do
gzip ${o}/${sn}/${sn}_complex_data/rep${repno}/${complex}/${complex}_rep${repno}_ch${chno}.nii
orient_corr ${o}/${sn}/${sn}_complex_data/rep${repno}/${complex}/${complex}_rep${repno}_ch${chno}
done
done
done


for repno in `seq 1 $no_reps`
do
for complex in real imag
do
for chno in `seq 1 $no_channels`
do
fslsplit ${o}/${sn}/${sn}_complex_data/rep${repno}/${complex}/${complex}_rep${repno}_ch${chno} ${o}/${sn}/${sn}_complex_data/rep${repno}/${complex}/tmp_${chno}_echo_
done
done
done


for complex in real imag
do
for chno in `seq 1 $no_channels`
do
for repno in 1
do
for VOL in `seq 1 ${LAST_ECHO}`
do
  MOV=`printf "%04d" ${VOL}`
  BASE=`printf "%04d" 0`
  flirt -applyxfm -ref ${o}/${sn}/${sn}_complex_data/rep1/${complex}/tmp_${chno}_echo_${BASE} -in ${o}/${sn}/${sn}_complex_data/rep${repno}/${complex}/tmp_${chno}_echo_${MOV} -init ${o}/${sn}/${sn}_reg/transforms_rep${repno}/${MOV}_to_${BASE}.mat -out ${o}/${sn}/${sn}_complex_data/rep${repno}/${complex}/tmp_${chno}_echo_${MOV}_resampled -interp spline
done
done
done
done

for repno in `seq 2 $no_reps`
do
for complex in real imag
do
for chno in 1 2 3 4
do
flirt -applyxfm -ref ${o}/${sn}/${sn}_complex_data/rep1/${complex}/tmp_${chno}_echo_0000 -in ${o}/${sn}/${sn}_complex_data/rep2/${complex}/tmp_${chno}_echo_0000 -init ${o}/${sn}/${sn}_reg/transforms_rep2/rep2_0000_to_rep1_0000.mat -out ${o}/${sn}/${sn}_complex_data/rep2/${complex}/tmp_${chno}_echo_0000_resampled -interp spline
done
done
done


for complex in real imag
do
for chno in `seq 1 $no_channels`
do
for repno in `seq 2 $no_reps`
do
for VOL in `seq 1 ${LAST_ECHO}`
do
  MOV=`printf "%04d" ${VOL}`
  BASE=`printf "%04d" 0`
  flirt -applyxfm -ref ${o}/${sn}/${sn}_complex_data/rep1/${complex}/tmp_${chno}_echo_${BASE} -in ${o}/${sn}/${sn}_complex_data/rep${repno}/${complex}/tmp_${chno}_echo_${MOV} -init ${o}/${sn}/${sn}_reg/transforms_rep${repno}/${MOV}_to_${BASE}_to_rep1_0000.mat -out ${o}/${sn}/${sn}_complex_data/rep${repno}/${complex}/tmp_${chno}_echo_${MOV}_resampled -interp spline
done
done
done
done


for chno in `seq 1 $no_channels`
do
for complex in real imag
do
for repno in 1
do
fslmerge -t ${o}/${sn}/${sn}_complex_data/rep${repno}/${complex}/${complex}_rep${repno}_${chno}_resampled  ${o}/${sn}/${sn}_complex_data/rep${repno}/${complex}/tmp_${chno}_echo_0000 `ls ${o}/${sn}/${sn}_complex_data/rep${repno}/${complex}/tmp_${chno}_echo_????_resampled.nii.gz`
done
done
done

for chno in `seq 1 $no_channels`
do
for complex in real imag
do
for repno in 2
do
fslmerge -t ${o}/${sn}/${sn}_complex_data/rep${repno}/${complex}/${complex}_rep${repno}_${chno}_resampled  `ls ${o}/${sn}/${sn}_complex_data/rep${repno}/${complex}/tmp_${chno}_echo_????_resampled.nii.gz`
done
done
done

for repno in `seq 1 $no_reps`
do
for complex in real imag
do
rm ${o}/${sn}/${sn}_complex_data/rep${repno}/${complex}/tmp_*
done
done
