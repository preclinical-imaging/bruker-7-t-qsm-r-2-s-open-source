#!/bin/bash

#qsm_pipeline_headers.sh
#
#
# Created by Cristiana Tisca on 12/05/2021

if [ "$1" == -h ]
then

echo "qsm_pipeline_headers.sh -sn <sample_name> -o <output_folder>"

exit
fi

while [ ! -z "$1" ]
do
case "$1" in
-sn) sn=${2};shift;;
-o) o=${2};shift;;
esac
#Prevent error using shift over 0 inputs
if [ $# -gt 0 ]
then
shift
fi
done

orient_corr () {
fslorient -deleteorient $1
fslswapdim $1 z -y x $1
fslorient -setsform 0.1 0 0 0 0 0.1 0 0 0 0 0.1 0 0 0 0 1 $1
fslorient -copysform2qform $1
fslorient -setsformcode 1 $1
fslorient -setqformcode 1 $1
}

gzip ${o}/${sn}/${sn}_qsm/${sn}_susceptibility.nii
gzip ${o}/${sn}/${sn}_t2s/${sn}_T2s.nii
gzip ${o}/${sn}/${sn}_t2s/${sn}_R2s.nii

orient_corr ${o}/${sn}/${sn}_qsm/${sn}_susceptibility.nii.gz
orient_corr ${o}/${sn}/${sn}_t2s/${sn}_T2s.nii.gz
orient_corr ${o}/${sn}/${sn}_t2s/${sn}_R2s.nii.gz
