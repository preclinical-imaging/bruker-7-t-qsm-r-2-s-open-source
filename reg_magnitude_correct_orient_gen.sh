#!/bin/bash

if [ "$1" == -h ]
then

echo "reg_magnitude_correct_orient_gen -o <output_file> -sn <sample_name> -no_reps <no_reps>"
echo "-magnitude_data_1 <magnitude_data_rep1>"
echo "Optional argument:"
echo "--magnitude_data_2 <magnitude_data_rep2>"
exit
fi

while [ ! -z "$1" ]
do
case "$1" in
-o) o=${2};shift;;
-sn) sn=${2};shift;;
-no_reps) no_reps=${2};shift;;
-magnitude_data_1) magnitude_data_1=${2};shift;;
--magnitude_data_2) magnitude_data_2=${2};shift;;
esac
#Prevent error using shift over 0 inputs
if [ $# -gt 0 ]
then
shift
fi
done

orient_corr () {
fslorient -deleteorient $1
fslswapdim $1 z -y x $1
fslorient -setsform 0.1 0 0 0 0 0.1 0 0 0 0 0.1 0 0 0 0 1 $1
fslorient -copysform2qform $1
fslorient -setsformcode 1 $1
fslorient -setqformcode 1 $1
}

#Copy data down.
for i in `seq 1 $no_reps`
do
  magnitude_var="magnitude_data_$i"
  cp "${!magnitude_var}"/*nii $o/$sn/${sn}_reg/${sn}_magnitude_data_raw_rep${i}.nii
done

#Orient data
for rep in `seq 1 $no_reps`
do
  gzip ${o}/${sn}/${sn}_reg/${sn}_magnitude_data_raw_rep${rep}.nii
  orient_corr ${o}/${sn}/${sn}_reg/${sn}_magnitude_data_raw_rep${rep}.nii.gz
  fslsplit ${o}/${sn}/${sn}_reg/${sn}_magnitude_data_raw_rep${rep}.nii ${o}/${sn}/${sn}_reg/tmp_echo_${rep}_
  mkdir ${o}/${sn}/${sn}_reg/transforms_rep${rep}
done

#Smooth original volume and split
for rep in `seq 1 $no_reps`
do
  fslmaths ${o}/${sn}/${sn}_reg/${sn}_magnitude_data_raw_rep${rep}.nii.gz -s 0.2 ${o}/${sn}/${sn}_reg/tmp_smooth
  fslsplit ${o}/${sn}/${sn}_reg/tmp_smooth ${o}/${sn}/${sn}_reg/tmp_echo_${rep}_smooth_
  rm ${o}/${sn}/${sn}_reg/tmp_smooth*.nii.gz
done

LAST_ECHO=`ls ${o}/${sn}/${sn}_reg/tmp_echo_1_????.* | wc -l`
LAST_ECHO=`expr ${LAST_ECHO} - 1`

#Run FLIRT for each pair of echoes, for the first repetition

for rep in `seq 1  $no_reps`
do
  for VOL in `seq 1 ${LAST_ECHO}`
    do
      REF=`expr ${VOL} - 1`
      REF=`printf "%04d" ${REF}`
      MOV=`printf "%04d" ${VOL}`
      flirt -dof 6 -cost leastsq -ref ${o}/${sn}/${sn}_reg/tmp_echo_${rep}_smooth_${REF} -in ${o}/${sn}/${sn}_reg/tmp_echo_${rep}_smooth_${MOV} -omat ${o}/${sn}/${sn}_reg/transforms_rep${rep}/${MOV}_to_${REF}.mat
    done
done

# Combine transforms for both reps
for rep in `seq 1 $no_reps`
do
  for VOL in `seq 2 ${LAST_ECHO}`
    do
      REF=`expr ${VOL} - 1`
      REF=`printf "%04d" ${REF}`
      MOV=`printf "%04d" ${VOL}`
      BASE=`printf "%04d" 0`
      convert_xfm -omat ${o}/${sn}/${sn}_reg/transforms_rep${rep}/${MOV}_to_${BASE}.mat -concat ${o}/${sn}/${sn}_reg/transforms_rep${rep}/${REF}_to_${BASE}.mat ${o}/${sn}/${sn}_reg/transforms_rep${rep}/${MOV}_to_${REF}.mat
      done
done

if [[ $no_reps -ne 1 ]];
then

    LAST_ECHO=`ls ${o}/${sn}/${sn}_reg/tmp_echo_1_????.* | wc -l`
    LAST_ECHO=`expr ${LAST_ECHO} - 1`

    for rep in `seq 2 $no_reps`
    do

      flirt -dof 6 -cost leastsq -ref ${o}/${sn}/${sn}_reg/tmp_echo_1_smooth_0000 -in ${o}/${sn}/${sn}_reg/tmp_echo_${rep}_smooth_0000 -omat ${o}/${sn}/${sn}_reg/transforms_rep${rep}/rep${rep}_0000_to_rep1_0000.mat

      for VOL in `seq 1 ${LAST_ECHO}`
      do
        #REF=`expr ${VOL} - 1`
        #REF=`printf "%04d" ${REF}`
        MOV=`printf "%04d" ${VOL}`
        BASE=`printf "%04d" 0`
        convert_xfm -omat ${o}/${sn}/${sn}_reg/transforms_rep${rep}/${MOV}_to_${BASE}_to_rep1_0000.mat -concat ${o}/${sn}/${sn}_reg/transforms_rep${rep}/rep${rep}_0000_to_rep1_0000.mat ${o}/${sn}/${sn}_reg/transforms_rep${rep}/${MOV}_to_${BASE}.mat
      done

      flirt -applyxfm -ref ${o}/${sn}/${sn}_reg/tmp_echo_1_0000 -in ${o}/${sn}/${sn}_reg/tmp_echo_${rep}_0000 -init ${o}/${sn}/${sn}_reg/transforms_rep${rep}/rep${rep}_0000_to_rep1_0000.mat -out ${o}/${sn}/${sn}_reg/tmp_echo_${rep}_0000_resampled -interp spline

      for VOL in `seq 1 ${LAST_ECHO}`
      do
        MOV=`printf "%04d" ${VOL}`
        BASE=`printf "%04d" 0`
        flirt -applyxfm -ref ${o}/${sn}/${sn}_reg/tmp_echo_1_${BASE} -in ${o}/${sn}/${sn}_reg/tmp_echo_${rep}_${MOV} -init ${o}/${sn}/${sn}_reg/transforms_rep${rep}/${MOV}_to_${BASE}_to_rep1_0000.mat -out ${o}/${sn}/${sn}_reg/tmp_echo_${rep}_${MOV}_resampled -interp spline
      done

      fslmerge -t ${o}/${sn}/${sn}_reg/${sn}_rep${rep} `ls ${o}/${sn}/${sn}_reg/tmp_echo_${rep}_????_resampled.nii.gz`

    done

    for rep in 1
    do
    for VOL in `seq 1 ${LAST_ECHO}`
    do
      MOV=`printf "%04d" ${VOL}`
      BASE=`printf "%04d" 0`
      flirt -applyxfm -ref ${o}/${sn}/${sn}_reg/tmp_echo_${rep}_${BASE} -in ${o}/${sn}/${sn}_reg/tmp_echo_${rep}_${MOV} -init ${o}/${sn}/${sn}_reg/transforms_rep${rep}/${MOV}_to_${BASE}.mat -out ${o}/${sn}/${sn}_reg/tmp_echo_${rep}_${MOV}_resampled -interp spline
    done
    fslmerge -t ${o}/${sn}/${sn}_reg/${sn}_rep${rep} ${o}/${sn}/${sn}_reg/tmp_echo_${rep}_0000.nii.gz `ls ${o}/${sn}/${sn}_reg/tmp_echo_${rep}_????_resampled.nii.gz`

    done
else
  for rep in 1
  do
  for VOL in `seq 1 ${LAST_ECHO}`
  do
    MOV=`printf "%04d" ${VOL}`
    BASE=`printf "%04d" 0`
    flirt -applyxfm -ref ${o}/${sn}/${sn}_reg/tmp_echo_${rep}_${BASE} -in ${o}/${sn}/${sn}_reg/tmp_echo_${rep}_${MOV} -init ${o}/${sn}/${sn}_reg/transforms_rep${rep}/${MOV}_to_${BASE}.mat -out ${o}/${sn}/${sn}_reg/tmp_echo_${rep}_${MOV}_resampled -interp spline
  done
  fslmerge -t ${o}/${sn}/${sn}_reg/${sn}_rep${rep} ${o}/${sn}/${sn}_reg/tmp_echo_${rep}_0000.nii.gz `ls ${o}/${sn}/${sn}_reg/tmp_echo_${rep}_????_resampled.nii.gz`

  done
fi

if [[ $no_reps -ne 1 ]];
then
  cp ${o}/${sn}/${sn}_reg/${sn}_rep1.nii.gz ${o}/${sn}/${sn}_reg/${sn}_sum.nii.gz

  for rep in `seq 2 $no_reps`
  do
  fslmaths ${o}/${sn}/${sn}_reg/${sn}_sum.nii.gz -add ${o}/${sn}/${sn}_reg/${sn}_rep${rep}.nii.gz ${o}/${sn}/${sn}_reg/${sn}_sum.nii.gz
  done
  fslmaths ${o}/${sn}/${sn}_reg/${sn}_sum -div $no_reps ${o}/${sn}/${sn}_reg/${sn}_avg.nii.gz
fi

rm ${o}/${sn}/${sn}_reg/tmp_echo_*.nii.gz
rm ${o}/${sn}/${sn}_reg/${sn}_sum.nii.gz
