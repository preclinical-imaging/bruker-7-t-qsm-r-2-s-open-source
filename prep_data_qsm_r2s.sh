#!/bin/bash

# qsm_pipeline_full.sh
#
#
# Created by Cristiana Tisca on 16/03/2022

module add mrdegibbs3d

if [ "$1" == -h ]
then

echo "prep_data_qsm_r2s"
echo "Optional arguments:"
echo "--config_file: configuration file; .cfg file. Example can be found in the pipeline's bin directory."
echo "This is an alternative to inputting the parameters directly into this command. If you want to do that,"
echo "then just manually input the parameters via the optional arguments below:"
echo "--o: output directory. This is where a folder with the name of your sample will be created."
echo "--sn: sample name. This is the name of your sample and the name of the directory which will be created."
echo "--no_reps: number of repetitions."
echo "--no_channels: number of channels."
echo "--no_TEs: number of echo times."
echo "--IES: inter-echo spacing."
echo "--voxelsize: voxelsize."
echo "--magnitude_data_1: folder with nifti file for the first repetition."
echo "--magnitude_data_2: folder with nifti file for the second repetition."
echo "--complex_data_1: folder with 2dseq file for the first repetition."
echo "--complex_data_2: folder with 2dseq file for second repetition."
echo "--brain_mask: nifti file with brain mask. Note that pipeline assumes a correctly oriented mask."
exit
fi

while [ ! -z "$1" ]
do
case "$1" in
--config_file) config_file=${2};shift;;
--o) o=${2};shift;;
--sn) sn=${2};shift;;
--no_reps) no_reps=${2};shift;;
--no_channels) no_channels=${2};shift;;
--no_TEs) no_TEs=${2};shift;;
--first_TE) first_TE=${2};shift;;
--IES) IES=${2};shift;;
--voxelsize) voxelsize=${2};shift;;
--magnitude_data_1) magnitude_data_1=${2};shift;;
--magnitude_data_2) magnitude_data_2=${2};shift;;
--complex_data_1) complex_data_1=${2};shift;;
--complex_data_2) complex_data_2=${2};shift;;
--mask) mask=${2};shift;;

esac
#Prevent error using shift over 0 inputs
if [ $# -gt 0 ]
then
shift
fi

done

if [[ -z $config_file ]];
then

  if [[ -z $o ]]
  then
    echo "Do not have output folder specified. Cannot continue."
  fi

  if [[ -z $sn ]]
  then
    echo "Do not have sample name specified. Cannot continue."
  fi

  if [[ -z $no_reps ]]
  then
    echo "Do not have number of repetitions specified. Cannot continue."
  fi

  if [[ -z $no_channels ]]
  then
    echo "Do not have number of channels specified. Cannot continue."
  fi

  if [[ -z $no_TEs ]]
  then
    echo "Do not have number of echo times specified. Cannot continue."
  fi

  if [[ -z $first_TE ]]
  then
    echo "Do not have first echo time specified. Cannot continue."
  fi

  if [[ -z $IES ]]
  then
    echo "Do not have inter echo spacing specified. Cannot continue."
  fi

  if [[ -z $voxelsize ]]
  then
    echo "Do not have voxelsize specified. Cannot continue."
  fi

  if [[ -z $magnitude_data_1 ]]
  then
    echo "Do not have folder for the first rep magnitude data specified. Cannot continue."
  fi

  if [[ -z $magnitude_data_2 ]]
  then
    echo "Do not have the folder for the second rep magnitude data specified."
    echo  "If you do not have a second repetition then pipeline will continue running without issues."
  fi

  if [[ -z $complex_data_1 ]]
  then
    echo "Do not have the folder for the first rep complex data specified. Cannot continue."
  fi

  if [[ -z $complex_data_2 ]]
  then
    echo "Do not have the folder for the second rep complex data specified."
    echo  "If you do not have a second repetition then pipeline will continue running without issues."
  fi

  if [[ -z $mask ]]
  then
    echo "Do not have the brain mask. Cannot continue."
  fi

else
  . $config_file
fi

mkdir ${o}/${sn} ${o}/${sn}/${sn}_reg ${o}/${sn}/${sn}_complex_data ${o}/${sn}/${sn}_qsm ${o}/${sn}/${sn}_t2s ${o}/${sn}/logs
touch ${o}/${sn}/logs/commands_prep_data_qsm_r2s
echo "mkdir ${o}/${sn} ${o}/${sn}/${sn}_reg ${o}/${sn}/${sn}_complex_data ${o}/${sn}/${sn}_qsm ${o}/${sn}/${sn}_t2s ${o}/${sn}/logs
touch ${o}/${sn}/logs/commands_prep_data_qsm_r2s" >> ${o}/${sn}/logs/commands_prep_data_qsm_r2s

echo Creating masterfiles for consequent processing.
bash bruker-7-t-qsm-r-2-s-open-source/bin/create_masterfiles.sh -no_reps $no_reps -no_channels $no_channels \
-voxelsize $voxelsize -no_TEs $no_TEs -first_TE $first_TE -IES $IES -sample_name $sn -output_folder $o/$sn
echo "bash bruker-7-t-qsm-r-2-s-open-source/bin/create_masterfiles.sh -no_reps $no_reps -no_channels $no_channels \
-no_TEs $no_TEs -first_TE $first_TE -IES $IES -output_folder $o/$sn" >> ${o}/${sn}/logs/commands_prep_data_qsm_r2s

echo Copying down complex data.
for rep in `seq $no_reps`
do
  mkdir $o/$sn/${sn}_complex_data/complex_data_rep${rep} $o/$sn/${sn}_complex_data/complex_data_rep${rep}/pdata $o/$sn/${sn}_complex_data/complex_data_rep${rep}/pdata/2dseq
  echo "mkdir $o/$sn/${sn}_complex_data/2dseq_rep${rep}" >> ${o}/${sn}/logs/commands_prep_data_qsm_r2s
  complex_var="complex_data_$rep"
  substring=${!complex_var%/*/*}
  echo ""complex_var="complex_data_$rep" >> ${o}/${sn}/logs/commands_prep_data_qsm_r2s
  cp ${!complex_var}/2dseq $o/$sn/${sn}_complex_data/complex_data_rep${rep}/pdata/2dseq/2dseq
  cp ${!complex_var}/reco $o/$sn/${sn}_complex_data/complex_data_rep${rep}/pdata/2dseq/reco
  cp ${!complex_var}/visu_pars $o/$sn/${sn}_complex_data/complex_data_rep${rep}/pdata/2dseq/visu_pars
  cp $substring/method $o/$sn/${sn}_complex_data/complex_data_rep${rep}/method
  cp $substring/acqp $o/$sn/${sn}_complex_data/complex_data_rep${rep}/acqp
  echo "cp ${!complex_var}/2dseq $o/$sn/${sn}_complex_data/complex_data_rep${rep}/pdata/2dseq/2dseq \
  cp ${!complex_var}/reco $o/$sn/${sn}_complex_data/complex_data_rep${rep}/pdata/2dseq/reco \
  cp ${!complex_var}/visu_pars $o/$sn/${sn}_complex_data/complex_data_rep${rep}/pdata/2dseq/reco \
  cp $substring/method $o/$sn/${sn}_complex_data/complex_data_rep${rep}/method \
  cp $substring/acqp $o/$sn/${sn}_complex_data/complex_data_rep${rep}/acqp" \
   >> ${o}/${sn}/logs/commands_prep_data_qsm_r2s
done

### Note that WIN's cluster runs the Grid Engine (GE) queuing software (using the Son of Grid Engine distribution). To ease job submission we use a helper called fsl_sub​ which sets some useful options to Grid Engine's built-in qsub​ command. You will need to change the job submission lines depending on your cluster's requirements.
echo Running magnitude data registration.
jid1=`fsl_sub -q short.q -l ${o}/${sn}/logs bruker-7-t-qsm-r-2-s-open-source/reg_magnitude_correct_orient_gen.sh \
-o $o -sn $sn -no_reps $no_reps -magnitude_data_1 $magnitude_data_1 --magnitude_data_2 $magnitude_data_2`
echo "fsl_sub -q short.q -l ${o}/${sn}/logs bruker-7-t-qsm-r-2-s-open-source/reg_magnitude_correct_orient_gen.sh \
-o $o -sn $sn -no_reps $no_reps" >> ${o}/${sn}/logs/commands_prep_data_qsm_r2s
echo Jobid $jid1

echo Reading 2dseq complex data.
jid2=`fsl_sub -q veryshort.q -j $jid1 -l ${o}/${sn}/logs -s openmp,4 \
matlab -singleCompThread -nodisplay -nosplash -r \
"addpath('bruker-7-t-qsm-r-2-s-open-source/QSMprocessing');get_real_imag_gen('$o','$sn',$voxelsize, $no_reps, $no_channels)"`
echo "fsl_sub -q veryshort.q -j $jid1 -l ${o}/${sn}/logs -s openmp,4 \
matlab -singleCompThread -nodisplay -nosplash -r \
addpath('bruker-7-t-qsm-r-2-s-open-source/QSMprocessing');get_real_imag_gen('$o','$sn',$voxelsize, $no_reps, $no_channels) " >> ${o}/${sn}/logs/commands_prep_data_qsm_r2s
echo $jid2

echo Running complex data registration.
if [[ $no_reps -ne 1 ]];
then
  jid3=`fsl_sub -q short.q -j $jid2 -l ${o}/${sn}/logs \
  bruker-7-t-qsm-r-2-s-open-source/register_complex_correct_orient_gen_v2.sh -o $o -sn $sn -no_channels $no_channels -no_reps $no_reps -no_TEs $no_TEs`
  echo "fsl_sub -q short.q -j $jid2 -l ${o}/${sn}/logs \
  bruker-7-t-qsm-r-2-s-open-source/register_complex_correct_orient_gen_v2.sh -o $o -sn $sn -no_channels $no_channels -no_reps $no_reps -noTEs $no_TEs" >> ${o}/${sn}/logs/commands_prep_data_qsm_r2s
else
  jid3=`fsl_sub -q short.q -j $jid2 -l ${o}/${sn}/logs \
 bruker-7-t-qsm-r-2-s-open-source/register_complex_correct_orient_onerep.sh -o $o -sn $sn -no_channels $no_channels`
  echo "fsl_sub -q short.q -j $jid2 -l ${o}/${sn}/logs \
  bruker-7-t-qsm-r-2-s-open-source/register_complex_correct_orient_onerep.sh -o $o -sn $sn -no_channels $no_channels" >> ${o}/${sn}/logs/commands_prep_data_qsm_r2s
fi
echo Jobid $jid3

echo Running 3D Gibbs ringing correction.
if [[ $no_reps -ne 1 ]];
then
  jid4=`fsl_sub -q short.q -j $jid3 -l ${o}/${sn}/${sn}_reg/gibbslogs deGibbs3D ${o}/${sn}/${sn}_reg/${sn}_avg.nii.gz ${o}/${sn}/${sn}_reg/${sn}_avg_gibbs.nii.gz`
  echo "fsl_sub -q short.q -j $jid3 -l ${o}/${sn}/${sn}_reg/gibbslogs deGibbs3D ${o}/${sn}/${sn}_reg/${sn}_avg.nii.gz ${o}/${sn}/${sn}_reg/${sn}_avg_gibbs.nii.gz" \
  >> ${o}/${sn}/logs/commands_prep_data_qsm_r2s
else
  jid4=`fsl_sub -q short.q -j $jid3 -l ${o}/${sn}/${sn}_reg/gibbslogs deGibbs3D ${o}/${sn}/${sn}_reg/${sn}_rep1.nii.gz ${o}/${sn}/${sn}_reg/${sn}_rep1_gibbs.nii.gz`
  echo "fsl_sub -q short.q -j $jid3 -l ${o}/${sn}/${sn}_reg/gibbslogs deGibbs3D ${o}/${sn}/${sn}_reg/${sn}_rep1.nii.gz ${o}/${sn}/${sn}_reg/${sn}_rep1_gibbs.nii.gz"
fi
echo Jobid $jid4
