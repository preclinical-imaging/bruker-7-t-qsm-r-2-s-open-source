#!/bin/bash

# qsm_pipeline_full.sh
#
#
# Created by Cristiana Tisca on 16/03/2022

module add mrdegibbs3d

if [ "$1" == -h ]
then

echo "qsm_pipeline_data_prepped.sh -o <directory> -sn <sample_name> -mask <brain_mask>"
echo "Optional arguments:"
echo "--MCPC_3D: runs alternative coil-combination method (Eckstein et al,2018)"
echo "--no_cleanup: keeps intermediary files produced in the pipeline. Useful for debugging."
echo "--first_TE: first echo time, can input directly if config_file/masterfiles folder not present."
echo "--IES: inter-echo spacing, can input directly if config_file/masterfiles folder not present."
echo "--no_channels: number of channels, can input directly if config_file/masterfiles folder not present."
echo "--no_reps: number of repetitions, can input directly if config_file/masterfiles folder not present."
echo "--no_TEs: number of echo times, can input directly if config_file/masterfiles folder not present."
echo "--voxelsize: voxelsize, can input directly if config_file/masterfiles folder not present."

exit
fi

while [ ! -z "$1" ]
do
case "$1" in
-o) o=${2};shift;;
-sn) sn=${2};shift;;
-mask) mask=${2};shift;;
--MCPC_3D)  coilcomb=mcpc_3d;;
--no_cleanup) no_cleanup=yes;;
--first_TE) first_TE=${2};shift;;
--IES) IES=${2};shift;;
--no_channels) no_channels=${2};shift;;
--no_reps) no_reps=${2};shift;;
--no_TEs) no_TEs=${2};shift;;
--voxelsize) voxelsize=${2};shift;;
esac
#Prevent error using shift over 0 inputs
if [ $# -gt 0 ]
then
shift
fi

done

if [[ -z $first_TE ]];
then
  first_TE=$( cat $o/$sn/masterfiles/first_TE )
else
  echo "Do not have first echo time parameter, first_TE. Cannot continue."
fi

if [[ -z $IES ]];
then
  IES=$( cat $o/$sn/masterfiles/IES )
else
  echo "Do not have inter echo space parameter, IES. Cannot continue."
fi

if [[ -z $no_channels ]];
then
  no_channels=$( cat $o/$sn/masterfiles/no_channels )
else
  echo "Do not have number of channels parameter, no_channels. Cannot continue."
fi

if [[ -z $no_reps ]];
then
  no_reps=$( cat $o/$sn/masterfiles/no_reps )
else
  echo "Do not have number of channels repetitions parameter, no_reps. Cannot continue."
fi

if [[ -z $no_TEs ]];
then
  no_TEs=$( cat $o/$sn/masterfiles/no_TEs )
else
  echo "Do not have number of echo times repetitions parameter, no_TEs. Cannot continue."
fi

if [[ -z $voxelsize ]];
then
  voxelsize=$( cat $o/$sn/masterfiles/voxelsize )
else
  echo "Do not have voxelsize parameter, voxelsize. Cannot continue."
fi


touch ${o}/${sn}/logs/commands_data_prepped_qsm_r2s

for repno in `seq ${no_reps}`
do
  mkdir $o/$sn/post_processing_${sn}_rep$repno
  mkdir $o/$sn/post_processing_${sn}_rep$repno/post_processing
  mkdir $o/$sn/post_processing_${sn}_rep$repno/logs
done

### Note that WIN's cluster runs the Grid Engine (GE) queuing software (using the Son of Grid Engine distribution). To ease job submission we use a helper called fsl_sub​ which sets some useful options to Grid Engine's built-in qsub​ command. You will need to change the job submission lines depending on your cluster's requirements.

echo Queuing QSM pipeline steps.
if [[ -z $coilcomb ]];
then
  echo Step 1: Queuing coil combination and phase offset removal: default method.
  jid1=`fsl_sub -q veryshort.q \
  -l ${o}/${sn}/logs -s openmp,4 matlab -singleCompThread -nodisplay \
  -nosplash -r " addpath('bruker-7-t-qsm-r-2-s-open-source/QSMprocessing');qsm_step1_gen('${o}/${sn}/','${sn}', $no_TEs, $no_channels, $no_reps, $voxelsize)"`
  echo "fsl_sub -q veryshort.q \
  -l ${o}/${sn}/logs -s openmp,4 matlab -singleCompThread -nodisplay \
  -nosplash -r \
  addpath('bruker-7-t-qsm-r-2-s-open-source/QSMprocessing'); \
  qsm_step1_gen('${o}/${sn}/','${sn}', $no_TEs, $no_channels, $no_reps, $voxelsize)" >> ${o}/${sn}/logs/commands_data_prepped_qsm_r2s
else
  echo Step 1: Queuing coil combination: MCPC_3D method.
  jid1=`fsl_sub -q veryshort.q \
  -l ${o}/${sn}/logs -s openmp,4 matlab -singleCompThread -nodisplay \
  -nosplash -r " addpath('bruker-7-t-qsm-r-2-s-open-source/QSMprocessing');qsm_step1_gen_MCPC_3D('${o}/${sn}/','${sn}', $no_TEs, $first_TE, $IES, $no_coils, $no_reps, $voxelsize)"`
  echo "fsl_sub -q veryshort.q \
  -l ${o}/${sn}/logs -s openmp,4 matlab -singleCompThread -nodisplay \
  -nosplash -r \
  addpath('bruker-7-t-qsm-r-2-s-open-source/QSMprocessing');\
  qsm_step1_gen_MCPC_3D('${o}/${sn}/','${sn}', $no_TEs, $first_TE, $IES, $no_coils, $no_reps, $voxelsize)"
fi
echo Jobid $jid1

echo Step 2: Queuing field map estimation.
jid2=`bash bruker-7-t-qsm-r-2-s-open-source/QSMprocessing/QSM_fieldfitting.sh \
-o  ${o}/${sn}/ -no_reps $no_reps -voxelsize $voxelsize -no_TEs $no_TEs -first_TE $first_TE \
-IES $IES -j $jid1`
echo "bash bruker-7-t-qsm-r-2-s-open-source/QSMprocessing/QSM_fieldfitting.sh \
-o  ${o}/${sn}/ -no_reps $no_reps -voxelsize $voxelsize -no_TEs $no_TEs -first_TE $first_TE \
-IES $IES -j $jid1" >> ${o}/${sn}/logs/commands_data_prepped_qsm_r2s
echo Jobid $jid2

echo Step 3: Queuing background field removal and dipole inversion.
jid3=`fsl_sub -q veryshort.q -j $jid2 \
-l ${o}/${sn}/logs -s openmp,4 matlab -singleCompThread -nodisplay -nosplash \
-r "addpath('bruker-7-t-qsm-r-2-s-open-source/QSMprocessing');qsm_step2_gen('${o}/${sn}/', \
'${mask}', '${sn}', $no_reps, $voxelsize)"`
echo "fsl_sub -q veryshort.q -j $jid2 \
-l ${o}/${sn}/logs -s openmp,4 matlab -singleCompThread -nodisplay -nosplash \
-r addpath('bruker-7-t-qsm-r-2-s-open-source/QSMprocessing');qsm_step2_gen('${o}/${sn}/', \
'${mask}', '${sn}', $no_reps, $voxelsize)" >> ${o}/${sn}/logs/commands_data_prepped_qsm_r2s
echo Jobid $jid3

echo Queuing T2s pipeline steps.
jid4=`bash bruker-7-t-qsm-r-2-s-open-source/QSMprocessing/T2s_pipeline.sh -j $jid3 -o ${o} -sn ${sn} -no_reps $no_reps \
-voxelsize $voxelsize -no_TEs $no_TEs -first_TE $first_TE -IES $IES -mask ${mask}`
echo "bash bruker-7-t-qsm-r-2-s-open-source/QSMprocessing/T2s_pipeline.sh -j $jid3 -o ${o} -sn ${sn} -no_reps $no_reps \
-voxelsize $voxelsize -no_TEs $no_TEs -first_TE $first_TE -IES $IES -mask ${mask}"  >> ${o}/${sn}/logs/commands_data_prepped_qsm_r2s
echo Jobid $jid4

echo Queuing header correction.
jid5=`fsl_sub -q veryshort.q -j $jid4 -l ${o}/${sn}/logs bruker-7-t-qsm-r-2-s-open-source/qsm_pipeline_headers.sh -o ${o} -sn ${sn}`
echo "fsl_sub -q veryshort.q -j $jid4 -l ${o}/${sn}/logs bruker-7-t-qsm-r-2-s-open-source/qsm_pipeline_headers.sh -o ${o} -sn ${sn}" \
>> ${o}/${sn}/logs/commands_data_prepped_qsm_r2s
echo Jobid $jid5

if [[ -z $no_cleanup ]];
then
  echo Queuing cleanup script.
  jid6=`fsl_sub -q veryshort.q -j $jid5 -l ${o}/${sn}/logs bruker-7-t-qsm-r-2-s-open-source/qsm_pipeline_cleanup.sh \
  -o $o -sn $sn`
  echo "fsl_sub -q veryshort.q -j $jid5 -l ${o}/${sn}/logs bruker-7-t-qsm-r-2-s-open-source/qsm_pipeline_cleanup.sh \
  -o $o -sn $sn" >> ${o}/${sn}/logs/commands_data_prepped_qsm_r2s
  echo Jobid $jid6
fi
