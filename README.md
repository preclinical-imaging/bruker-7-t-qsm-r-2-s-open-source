# WIN Bruker 7T Ex Vivo Multi-Echo Gradient Echo (for QSM/R2*) Post-Processing Pipeline 

This repository  contains all scripts to run the ex vivo QSM/R2* post-processing pipeline on data acquired at WIN's 7T Bruker facility. It should be compatible with any other data acquired on a similar Bruker scanner using an equivalent protocol.

This resource contains anonymised file-paths which will need to be edited to enable running on a cluster facility. The commands for submitting jobs to the cluster also need to be edited.

This pipeline is based on the code developed by Chaoyue Wang and Benjamin Tendler and published here: https://doi.org/10.1016/j.neuroimage.2020.117216. The scripts were either written or adapted by Cristiana Tisca.

The outputs of the pipeline include QSM and R2* maps. 

This code is maintained by Cristiana Tisca.

Cristiana Tisca

cristiana.tisca@linacre.ox.ac.uk, cristiana.tisca@yahoo.com

June 2023

---
### HOW TO CITE THIS MATERIAL
DOI: 10.5281/zenodo.8130910


---
### SOFTWARE REQUIREMENTS

* `FSL` (`prelude`, `FLIRT`)
* `mrdegibbs3D` (https://github.com/jdtournier/mrdegibbs3D)
* `MATLAB R2019a` or newer
* `MATLAB` `read_2dseq` function for reading in Bruker 2dseq files: https://www.mathworks.com/matlabcentral/fileexchange/69177-read_2dseq-quickly-reads-bruker-s-2dseq-mri-images
* `MATLAB` "Tools for NIftI and ANALYZE image" Toolbox: https://www.mathworks.com/matlabcentral/fileexchange/8797-tools-for-nifti-and-analyze-image 
* `MATLAB` "STI Suite" Toolbox: https://people.eecs.berkeley.edu/~chunlei.liu/software.html


---
### NOTES ON DATA ACQUISITION

This pipeline makes, at various points, assumptions about the format of the data (e.g. the specific orientation correction done for the nifti files). Please adjust these files depending on your requirements.  See below for a list of adjustments that need to be made to run the pipeline on your cluster.

Our multi-modal ex vivo MRI data acquisition parameters (including dMRI, multi-echo GRE and structural T2-weighted MRI) can be found in `acq_params.md`.


---
### DIAGRAM OF THE PIPELINE

<p align="center">
<img src="Pipeline_Guide_and_QC/Photos/Fig4.7.QSM_R2s_pipelines.png" alt="Diagram of the QSM and R2* post-processing pipelines"
	title="Diagram of the QSM and R2* processing pipeline" width="700" height="400" />

Diagram of the QSM processing pipeline, adapted from Wang and colleagues (C. Wang et al., 2020). TE: echo time; QSM: Quantitative Susceptibility Map.  
</p>


---
### PUBLISHED WORK USING THIS PIPELINE

* OHBM Annual Meeting. Cristiana Tisca, Mohamed Tachrount, Adele Smart, Frederik J Lange, Amy FD Howard, Chaoyue Wang, Lily Qiu, Benjamin Tendler, Claire Bratley, Daniel ZL Kor, Istvan N Huszar, Javier Ballarobre-Barreiro, Marika Fava, Manuel Mayr, Jason P Lerch, Aurea B Martins-Bach, Karla L Miller. Linking MRI to histology in the mouse brain: A framework for data acquisition and pre-processing. July 2023
* ISMRM & ESMRMB Joint meeting. Cristiana Tisca, Mohamed Tachrount, Frederik J Lange, Amy FD Howard, Chaoyue Wang, Lily R Qiu, Benjamin C Tendler, Jason P Lerch, Aurea B Martins-Bach, Karla L Miller. White matter microstructure changes in a Bcan knockout mouse model. May 2022 
* ISMRM & SMRT Virtual Conference. Cristiana Tisca, Mohamed Tachrount, Chaoyue Wang, Lily Qiu, Javier Barallobre-Barreiro, Marika Fava, Manuel Mayr, Jason Lerch, Aurea B Martins-Bach, Karla L Miller. Vcan mutation leads to sex-specific changes in white matter microstructure in mice. Apr 2021


---
### WIN RODENT EX VIVO MULTI-ECHO GRADIENT ECHO DATA ACQUISITION DETAILS

* the MGE datasets consists of two separate repetitions, usually acquired at the end of the overnight scanning session after the second diffusion shell (but some datasets have been subsequently reacquired separately after adjustments to the acquisition protocol at the end of March 2021) 

* each repetition consists of 20 volumes acquired at echo times between 3-60ms (inter-echo intervals: 3ms). Adjustments before each repetitions include: ATS position, basic frequency, receiver gain, scan shim. Repetitions also have prospective navigator correction (to deal with the frequency drift due to temperature fluctuations of the passive shim – intrinsic to our system. 

* additional details about scanning parameters:  sequence – 3D multi-echo GRE, FA=15deg, TE1=3ms, TR=68ms, BW=250kHz 

* initial steps include the registration of all echoes to the first echo of the first repetition; this is done initially for the magnitude data and the resulting transforms are applied to the complex data as well; this is followed by Gibbs ringing correction (using the method by Bautista et al, 2021, based on Kellner et al, 2018) 

* for the quantitative susceptibility mapping (QSM) pipeline: the complex data is then coil-combined (using one of two methods implemented within the pipeline), followed by field map estimation, background field removal and dipole inversion. The combination for both repetitions is done by averaging the fitted field maps prior to background field removal. 

* for the R2s/T2s pipeline: the registered and Gibbs-corrected magnitude images are used to estimate R2s/T2s. The combination for both repetitions is done by averaging the magnitude data prior to fitting.

* You need to have brain masks ready before you run the pipeline. The final QSM outputs are going to be affected by the presence of non-brain voxels, particularly bubbles. This preliminary step should be performed in order to ensure that your maps are of good quality. 

* You can use the first echo of the first repetition, which you can feed into an atlas based segmentation algorithm (e.g. MBM.py pipeline from the Mouse Imaging Centre). Due to the reduced contrast of the first echo image, you may run into issues where brain masks for some samples are not accurate (e.g. they can be bigger/smaller/weirdly shaped). If you need a later-echo volume (e.g. I have used echo 6), then you first need to perform a registration step to the first volume.  

---
### ADJUSTMENTS NEEDED TO RUN THE PIPELINE ON YOUR LOCAL CLUSTER

*  WIN's cluster runs using the Grid Engine (GE) queuing software (using the Son of Grid Engine distribution). To ease job submission we use a helper called fsl_sub​ which sets some useful options to Grid Engine's built-in qsub​ command. You will need to change/edit out all the job submission lines depending on your cluster's requirements.
* We perform a reorientation of the dimensions nifti files into correct rodent neuroanatomical dimensions using the function `orient_corr`. You might have to adapt this function based on the orientation of the nifti files outputted by your scanner software.
* for all `MATLAB` scripts, add path to installed functions (`read_2dseq`,"Tools for NIftI and ANALYZE image" Toolbox )

---
### BIBLIOGRAPHY

* Bautista, T. et al. (2021) ‘Removal of Gibbs ringing artefacts for 3D acquisitions using subvoxel shifts’, in Proc. Intl. Soc. Mag. Reson. Med., p. 3535.
* Chakravarty, M. M. et al. (2013) ‘Performing label-fusion-based segmentation using multiple automatically generated templates’, Human Brain Mapping. doi: 10.1002/hbm.22092.
* Eckstein, K. et al. (2018) ‘Computationally Efficient Combination of Multi-channel Phase Data From Multi-echo Acquisitions (ASPIRE)’, Magnetic Resonance in Medicine, 79(6), pp. 2996–3006. doi: 10.1002/mrm.26963.
* Friedel, M. et al. (2014) ‘Pydpiper: A flexible toolkit for constructing novel registration pipelines’, Frontiers in Neuroinformatics. doi: 10.3389/fninf.2014.800067.
* Gudbjartsson, H. and Patz, S. (1995) ‘The rician distribution of noisy mri data’, Magnetic Resonance in Medicine, 34(6), pp. 910–914. doi: 10.1002/mrm.1910340618.
* Jenkinson, M. (2003) ‘Fast, automated, N-dimensional phase-unwrapping algorithm’, Magnetic Resonance in Medicine, 49(1), pp. 193–197. doi: 10.1002/mrm.10354.
* Kellner, E. et al. (2016) ‘Gibbs-ringing artifact removal based on local subvoxel-shifts’, Magnetic Resonance in Medicine. doi: 10.1002/mrm.26054.
* Li, W. et al. (2015) ‘A method for estimating and removing streaking artifacts in quantitative susceptibility mapping’, NeuroImage. doi: 10.1016/j.neuroimage.2014.12.043.
* Li, W., Wu, B. and Liu, C. (2011) ‘Quantitative susceptibility mapping of human brain reflects spatial variation in tissue composition’, NeuroImage. doi: 10.1016/j.neuroimage.2010.11.088.
* Robinson, S. D. et al. (2017) ‘Combining phase images from array coils using a short echo time reference scan (COMPOSER)’, Magnetic Resonance in Medicine. doi: 10.1002/mrm.26093.
* Schweser, F. et al. (2011) ‘Quantitative imaging of intrinsic magnetic tissue properties using MRI signal phase: An approach to in vivo brain iron metabolism?’, NeuroImage. doi: 10.1016/j.neuroimage.2010.10.070.
* Tisca, C. et al. (2021) ‘Vcan mutation induces sex-specific changes in white matter microstructure in mice’, in Proc. Intl. Soc. Mag. Reson. Med. 29, p. 1226. Available at: https://index.mirasmart.com/ISMRM2021/PDFfiles/1226.html.
* Tisca, C. et al. (2022) ‘White matter microstructure changes in a Bcan knockout mouse model’, in Proc. Intl. Soc. Mag. Reson. Med. 31.
* Wang, C. et al. (2020) ‘Methods for quantitative susceptibility and R2* mapping in whole post-mortem brains at 7T applied to amyotrophic lateral sclerosis’, NeuroImage. Elsevier Inc., 222(May), p. 117216. doi: 10.1016/j.neuroimage.2020.117216.
* Wang, C. et al. (2022) ‘Phenotypic and genetic associations of quantitative magnetic susceptibility in UK Biobank brain imaging’, Nature Neuroscience, 25(6), pp. 818–831. doi: 10.1038/s41593-022-01074-w.





