#!/bin/bash

# check_complex_recon.sh
#
#
# Created by Cristiana Tisca on 1/07/2022



if [ "$1" == -h ]
then

echo "check_complex_recon"
echo "Argument:"
echo "-recon_2dseq_folder: folder where the 2dseq data is."
echo "-output_folder: folder where the data will be saved."
echo "This script will read in the 2dseq data and output a nifti file representing the first volume of the first coil."
echo "This nifti file can be used to check whether the complex data reconstruction was done accurately."
echo "It will also output a data_check.txt that will indicate whether the required number of volumes are present in the 2dseq file,"
echo "as well as the data type of each cell."
echo "Note this function was designed for the BSB ex vivo MGE protocol so it will be checking that data is as expected for that"
echo "particular protocol."

exit
fi

while [ ! -z "$1" ]
do
case "$1" in
-recon_2dseq_folder) recon_2dseq_folder=${2};shift;;
-output_folder) output_folder=${2};shift;;
esac
#Prevent error using shift over 0 inputs
if [ $# -gt 0 ]
then
shift
fi
done

mkdir $output_folder
mkdir $output_folder/logs

### Note that WIN's cluster runs the Grid Engine (GE) queuing software (using the Son of Grid Engine distribution). To ease job submission we use a helper called fsl_sub​ which sets some useful options to Grid Engine's built-in qsub​ command. You will need to change the job submission lines depending on your cluster's requirements.

fsl_sub -q veryshort.q -l $output_folder/logs -s openmp,4 \
matlab -singleCompThread -nodisplay -nosplash -r \
addpath('bruker-7-t-qsm-r-2-s-open-source/QSMprocessing');check_complex_recon('${recon_2dseq_folder}','${output_folder}')"
