#!/bin/bash

#qsm_pipeline_headers.sh
#
#
# Created by Cristiana Tisca on 12/05/2021

if [ "$1" == -h ]
then

echo "qsm_pipeline_cleanup.sh -sn <sample_name> -o <output_folder>"

exit
fi

while [ ! -z "$1" ]
do
case "$1" in
-sn) sn=${2};shift;;
-o) o=${2};shift;;
esac
#Prevent error using shift over 0 inputs
if [ $# -gt 0 ]
then
shift
fi
done

mkdir ${o}/${sn}/qsm_r2s_parametric_maps
mv ${o}/${sn}/${sn}_qsm/${sn}_susceptibility.nii.gz ${o}/${sn}/qsm_r2s_parametric_maps/susceptibility.nii.gz
mv ${o}/${sn}/${sn}_t2s/${sn}_T2s.nii.gz ${o}/${sn}/qsm_r2s_parametric_maps/T2s.nii.gz
mv ${o}/${sn}/${sn}_t2s/${sn}_R2s.nii.gz ${o}/${sn}/qsm_r2s_parametric_maps/R2s.nii.gz

rm -rf ${o}/${sn}/*${sn}*
rm -rf ${o}/${sn}/logs/matlab* ${o}/${sn}/logs/qsm* ${o}/${sn}/logs/reg*
