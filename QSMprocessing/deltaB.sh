#!/bin/sh

#  deltaB.sh
#
#
#  Created by Chaoyue Wang on 05/07/2018.
#

while [ ! -z "$1" ]
do
case "$1" in
-i) input=${2};shift;;
-j) jobid=${2};shift;;
esac
#Prevent error using shift over 0 inputs
if [ $# -gt 0 ]
then
shift
fi
done


echo "prelude -a $input/post_processing/mask.nii -p $input/post_processing/cd1.nii \
-u $input/post_processing/UWcd1c -m $input/post_processing/mask.nii" >> $input/logs/commands_deltaB

echo "prelude -a $input/post_processing/mask.nii -p $input/post_processing/phasec1.nii \
-u $input/post_processing/UWphasec1c -m $input/post_processing/mask.nii" >> $input/logs/commands_deltaB

echo "gunzip ('$input/post_processing/UWcd1c.nii.gz')" >> $input/ungzfordBfitting.m
echo "gunzip ('$input/post_processing/UWphasec1c.nii.gz')" >> $input/ungzfordBfitting.m


jobid12=`fsl_sub -q short.q -l $input/logs/ -j $jobid -t $input/logs/commands_deltaB`


gzid99=`fsl_sub -q veryshort.q -l $input/logs/ -j $jobid12 matlab -singleCompThread \
-nodisplay -nosplash \< $input/ungzfordBfitting.m`

echo $gzid99
