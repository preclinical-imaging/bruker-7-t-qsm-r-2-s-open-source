function qsm_step2_gen(folder_allregistered, brain_mask, sample_name, no_reps, voxelsize, varargin)
    
   
    load([folder_allregistered '/post_processing_' sample_name '_rep1/post_processing/rP0f.mat'])
    rdBf=zeros(size(rP0f_combined));
    for i=1:no_reps
        load([folder_allregistered '/post_processing_' sample_name '_rep' num2str(i) '/post_processing/rdBf.mat'])
    
        rdBf=rdBf_combined+rdBf;
    end 

    mask_intensity_threshold=niftiread([folder_allregistered '/post_processing_' sample_name '_rep1/post_processing/mask.nii']);
    mask_pipeline=niftiread(brain_mask);
    mask_pipeline=permute(mask_pipeline,[3 2 1]);
    mask_pipeline=flip(mask_pipeline,2);
    mask=mask_intensity_threshold.*mask_pipeline;
 
    
    rdBf=rdBf/no_reps;
    rdBft=rdBf*2*pi;
    
    dim=size(rdBf);

    for ii=1:dim(3)

        mask(:,:,ii)=bwareaopen(mask(:,:,ii),20);
        mask(:,:,ii)=~bwareaopen(~mask(:,:,ii),20);

    end


    %%your voxelsize here
    vx=voxelsize;

    vy=voxelsize;

    vz=voxelsize;
    
    [dB_vsf,mask_vsf]=V_SHARP(rdBft,mask,'voxelsize', [1 1 1], 'smvsize', 20);
 
    mask_vsf(rP0f_combined>1)=0;
    mask_vsf(rP0f_combined<-1)=0;
    
    niftiwrite(rdBf, [folder_allregistered '/' sample_name '_qsm/' sample_name '_rdBf'])
    niftiwrite(rP0f_combined, [folder_allregistered '/' sample_name '_qsm/' sample_name '_rP0f'])
    niftiwrite(dB_vsf, [folder_allregistered '/' sample_name '_qsm/' sample_name  '_dB_vsf'])
    
    B0=7;
    TE=1;
    H=[0 1 0];


    [susceptibility]= QSM_iLSQR(dB_vsf,mask_vsf,'TE',TE,'B0',B0,'H',H,'padsize',[6,6,6],'voxelsize',[vx vy vz]);
    nii=make_nii(susceptibility, [voxelsize voxelsize voxelsize], [],64);
    save_nii(nii, [folder_allregistered '/' sample_name '_qsm/' sample_name  '_susceptibility.nii']);
    
end

