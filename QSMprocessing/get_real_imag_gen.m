function get_real_imag_gen(main_folder, sample_name, voxelsize, no_reps, no_channels)

    
    
    for i=1:no_reps
        
        system(['mkdir -p ' main_folder '/' sample_name '/' sample_name '_complex_data/rep' num2str(i)]);  
        system(['mkdir -p ' main_folder '/' sample_name '/' sample_name '_complex_data/rep' num2str(i) '/real']);
        system(['mkdir -p ' main_folder '/' sample_name '/' sample_name '_complex_data/rep' num2str(i) '/imag']);
    
        data_mouse = read_2dseq([main_folder '/' sample_name '/' sample_name '_complex_data/complex_data_rep' num2str(i) '/pdata/2dseq']);

        data_mouse = double(data_mouse);
        data_mouse = squeeze(data_mouse);
        
        real_dm=real(data_mouse);
        imag_dm=imag(data_mouse);
        
        for j=1:no_channels
            
            nii=make_nii(real_dm(:,:,:,:,j), [voxelsize voxelsize voxelsize], [], 16);
            save_nii(nii,[main_folder '/' sample_name '/' sample_name '_complex_data/rep' num2str(i) '/real/real_rep' num2str(i) '_ch' num2str(j) '.nii']);
            
            nii=make_nii(imag_dm(:,:,:,:,j), [voxelsize voxelsize voxelsize], [], 16);
            save_nii(nii,[main_folder '/' sample_name '/' sample_name '_complex_data/rep' num2str(i) '/imag/imag_rep' num2str(i) '_ch' num2str(j) '.nii']);

        end
        
    end
    
 end
