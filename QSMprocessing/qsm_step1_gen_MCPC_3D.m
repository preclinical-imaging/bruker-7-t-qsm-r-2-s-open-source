function qsm_step1_gen_MCPC_3D(folder_allregistered, sample_name, no_TEs, first_TE, IES, no_coils, no_reps, voxelsize)
    

    for i=1:no_reps
        system(['mkdir -p ' folder_allregistered '/post_processing_' sample_name '_rep' num2str(i)]);
        system(['mkdir -p ' folder_allregistered '/post_processing_' sample_name '_rep' num2str(i) '/post_processing'])
        system(['mkdir -p ' folder_allregistered '/post_processing_' sample_name '_rep' num2str(i) '/logs'])
    end 
    
    for i=1:no_coils
        for j=1:no_reps
            data_mouse_real(:,:,:,:,i,j) = niftiread([folder_allregistered '/' sample_name '_complex_data/rep' num2str(j) '/real/real_rep' num2str(j) '_' num2str(i) '_resampled.nii.gz']);
            data_mouse_imag(:,:,:,:,i,j) = niftiread([folder_allregistered '/' sample_name '_complex_data/rep' num2str(j) '/imag/imag_rep' num2str(j) '_' num2str(i) '_resampled.nii.gz']);
        end
    end
    
    data_mouse=complex(data_mouse_real, data_mouse_imag);
    data_mouse=permute(data_mouse,[3 2 1 4 5 6]);
    data_mouse=flip(data_mouse,2);
    
    %MCPC-3D
    for i=1:no_reps

        S1=squeeze(data_mouse(:,:,:,1,:,1));
        S2=squeeze(data_mouse(:,:,:,2,:,1));
        TEs=[first_TE, first_TE+IES];

        dim = size(S1);

        hip = zeros(dim(1:3));
        for iCha = 1:size(S1, 4)
            complexDifference = S2(:, :, :, iCha) .* conj(S1(:, :, :, iCha));
            hip = hip + complexDifference;
        end

        clear complexDifference;

        hip_abs = abs(hip);
        hip_angle = angle(hip);

        nii=make_nii(hip_abs, [voxelsize voxelsize voxelsize], [], 64);
        save_nii(nii,[folder_allregistered '/post_processing_' sample_name '_rep' num2str(i) '/post_processing/hip_abs.nii']);

        nii=make_nii(hip_angle, [voxelsize voxelsize voxelsize], [], 64);
        save_nii(nii,[folder_allregistered '/post_processing_' sample_name '_rep' num2str(i) '/post_processing/hip_angle.nii']);

        temp=hip_abs;

        dim=size(temp);

        for k=1:dim(3)
            for j=1:dim(2)
               for ii=1:dim(1)

                    if temp(ii,j,k)<1e7
                        temp(ii,j,k)=0;
                    elseif temp(ii,j,k)>1e7
                        temp(ii,j,k)=1;
                    end

               end
            end
        end


        for ii=1:dim(3)

            temp(:,:,ii)=~bwareaopen(~temp(:,:,ii),10);
            temp(:,:,ii)=bwareaopen(temp(:,:,ii),30);

        end
        
        nii=make_nii(temp, [voxelsize voxelsize voxelsize], [], 64);
        save_nii(nii,[folder_allregistered '/post_processing_' sample_name '_rep' num2str(i) '/post_processing/mask.nii']);
        
    
        %prelude unwrapping

        system(['prelude -a ', folder_allregistered '/post_processing_' sample_name '_rep' num2str(i) '/post_processing', '/hip_abs.nii.gz -p ', ...
                folder_allregistered '/post_processing_' sample_name '_rep' num2str(i) '/post_processing', '/hip_angle.nii.gz -u ', folder_allregistered '/post_processing_' sample_name '_rep' num2str(i) '/post_processing', ...
                '/hip_uw.nii.gz -m ', folder_allregistered '/post_processing_' sample_name '_rep' num2str(i) '/post_processing', '/mask.nii']);
    
        unwrappedHip=double(niftiread([folder_allregistered '/post_processing_' sample_name '_rep' num2str(i) '/post_processing/hip_uw.nii.gz']));
    
        scale = TEs(1) / (TEs(2) - TEs(1));
        unwrappedHip = unwrappedHip * scale;

        hipComplex = exp(1i * unwrappedHip);

        size_compl = size(hipComplex);

        po = complex(zeros([size_compl(1:3) no_coils], 'double'));

        for iCha = 1:no_coils
           po_ang=angle(exp(1i * (angle(S1(:, :, :, iCha)) - unwrappedHip)));
           po_double=double(abs(S1(:, :, :, iCha)) .* exp(1i * po_ang));
           po(:, :, :, iCha) = po_double;
        end

        clear hip hip_abs hip_angle hipComplex po_ang po_double unwrappedHip scale;

        real_smmothed=double(zeros(size(po)));
        imag_smmothed=double(zeros(size(po)));

        for j=1:no_coils
            real_smmothed(:, :, :, j) = imgaussfilt3(real(po(:, :, :, j)), 4, ...
                                                     'FilterDomain', 'spatial');
            imag_smmothed(:, :, :, j) = imgaussfilt3(imag(po(:, :, :, j)), 4, ...
                                                     'FilterDomain', 'spatial');
        end

        clear po;

        po_c = (real_smmothed + 1i * imag_smmothed) ./ abs(real_smmothed + 1i * imag_smmothed);

        data_mouse_combined=zeros(size(data_mouse,1),size(data_mouse,2),size(data_mouse,3),size(data_mouse,4));
      
        for j=1:no_TEs
            S_c = squeeze(data_mouse(:,:,:,j,:,i)) .* squeeze(conj(po_c)); 
            data_mouse_combined(:,:,:,j) = weightedCombination(S_c, abs(S_c));
            data_mouse_combined(~isfinite(data_mouse_combined)) = 0;
        end
       
        
        phasec=angle(data_mouse_combined);   
        save([folder_allregistered '/post_processing_' sample_name '_rep' num2str(i) '/post_processing/phasec.mat'],'phasec','-v7.3');

        cd1=angle(exp(1i*(phasec(:,:,:,2)-phasec(:,:,:,1))));
        phasec1=phasec(:,:,:,1);

        nii=make_nii(cd1, [voxelsize voxelsize voxelsize], [],64);
        save_nii(nii,[folder_allregistered '/post_processing_' sample_name  '_rep' num2str(i) '/post_processing/cd1.nii']);
       
        data_rep=data_mouse_combined;
        save([folder_allregistered '/post_processing_' sample_name '_rep' num2str(i) '/post_processing/combined_data.mat'],'data_rep','-v7.3');

        nii=make_nii(phasec1, [voxelsize voxelsize voxelsize], [],64);
        save_nii(nii,[folder_allregistered '/post_processing_' sample_name '_rep' num2str(i) '/post_processing/phasec1.nii']);
    
    
    end 
    
end

