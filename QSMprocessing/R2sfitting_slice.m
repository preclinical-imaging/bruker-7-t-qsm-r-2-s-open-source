function R2sfitting_slice(INPUT, slice, nslices,  no_TEs, IES, first_TE)
%R2* fitting

M0m=load([INPUT 'M0est.mat']);

M0m=cell2mat(struct2cell(M0m));

magm=load([INPUT 'Magm.mat']);

magm=cell2mat(struct2cell(magm));

R2sm=load([INPUT 'R2sest.mat']);

R2sm=cell2mat(struct2cell(R2sm));

mask=load([INPUT 'R2s_mask.mat']);

mask=cell2mat(struct2cell(mask));

M0m=double(M0m);
magm=double(magm);
R2sm=double(R2sm);
mask=double(mask);

dim=size(R2sm);

PD=zeros(dim);
R2s=zeros(dim);
noise=zeros(dim);


if slice==1
    count_start=1;
else 
    count_start=(round(dim(3)/20)*(slice-1)+2);
end 

if slice==nslices
    count_end=dim(3);
else
    count_end=(round(dim(3)/20)*slice+1);
end 

for j=count_start:count_end
    for k=1:dim(1)
        for l=1:dim(2)
            
            if mask(k,l,j)==1
                
                t=1:no_TEs;
                y=squeeze([magm(k,l,j,:)])';
                TE=first_TE:IES:(no_TEs*IES);
                fun=@(A)(signal_ev_MGE(A(1),TE,A(2),A(3))-y(t));
                x0(1)=M0m(k,l,j);
                x0(2)=R2sm(k,l,j);
                x0(3)=100;
                
                options = optimoptions('lsqnonlin','Display','off');
                x=lsqnonlin(fun,x0,[0,0,0],[],options);
                
                PD(k,l,j)=x(1);
                R2s(k,l,j)=x(2);
                noise(k,l,j)=x(3);
                
            end
            
        end
    end
end

save([INPUT 'R2s_out' num2str(slice) '.mat'],'PD','R2s','noise')

end

