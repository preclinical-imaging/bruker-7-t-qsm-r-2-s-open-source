function deltaB_fitting_gen(INPUT, no_TEs, IES)
    
    
    
    nii=load_nii([INPUT '/post_processing/mask.nii']);
    mask=nii.img;

    nii=load_nii([INPUT '/post_processing/UWcd1c.nii']);
    cd1uw=nii.img;

    nii=load_nii([INPUT '/post_processing/UWphasec1c.nii']);
    phase1uw=nii.img;

    data=load([INPUT '/post_processing/combined_data.mat']);
    data=cell2mat(struct2cell(data));

    phasec=load([INPUT '/post_processing/phasec.mat']);
    phasec=cell2mat(struct2cell(phasec));

    % for all echo times (after subtraction of the first echo)
    for ii=1:(no_TEs-1)

        mag(:,:,:,ii)=abs(data(:,:,:,ii));

    end
    
    dB=cd1uw/(2*pi*IES*0.001);
    phi0=phase1uw-2*pi*dB*IES*0.001;


    % for all echo times (after subtraction of the first echo)    
    for ii=1:(no_TEs-1)

        magm(:,:,:,ii)=mag(:,:,:,ii).*mask;
        phasecm(:,:,:,ii)=phasec(:,:,:,ii).*mask;

    end


    dBm=double(dB.*mask);

    phi0m=double(phi0.*mask);

    phasecm=double(phasecm);

    magm=double(magm);


    save([INPUT '/post_processing/phasecm.mat'],'phasecm','-v7.3');

    save([INPUT '/post_processing/magm.mat'],'magm','-v7.3');

    save([INPUT '/post_processing/dBm.mat'],'dBm');

    save([INPUT '/post_processing/phi0m.mat'],'phi0m');


end
