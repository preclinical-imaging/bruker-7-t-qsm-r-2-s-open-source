function [M0,R2s] = R2sEst(mag,TE)


    dim=size(mag);

    TEm=mean(TE);
    TEm2=mean(TE.^2);
    TE=reshape(TE,[1,1,1,dim(4)]);
    TEmat=repmat(TE,[dim(1),dim(2),dim(3)]);%4D matrix

    testzero=prod(mag,4)==0;
    testzero=repmat(testzero,[1,1,1,dim(4)]);
    mag=log(mag);
    mag(testzero==1)=0;
    
    clear testzero;
    
    magm=mean(mag,4);
    
    convXYr2s = mean(TEmat.*mag,4)-TEm*magm;
    varX   = TEm2 - TEm^2;
    R2s = (-1).*convXYr2s/varX;
    M0  = exp(magm + R2s*TEm);

    R2s=real(R2s);
    M0=real(M0);

end

