function fittingcomb(INPUT,nslices)

    mask=niftiread([INPUT '/post_processing/mask.nii']);
   
    dim=size(mask);

    rdBf_combined=zeros(dim);
    rP0f_combined=zeros(dim);

    for i=1:nslices

        load([INPUT '/post_processing/fitting_out' num2str(i) '.mat'])

        if i==1
            count_start=1;
        else 
            count_start=(round(dim(3)/20)*(i-1)+2);
        end 

        if i==nslices
            count_end=dim(3);
        else
            count_end=(round(dim(3)/20)*i+1);
        end

        for ii=count_start:count_end

            rdBf_combined(:,:,ii)=rdBf(:,:,ii);
            rP0f_combined(:,:,ii)=rP0f(:,:,ii);
        end
    end 

    %save out data

    save([INPUT '/post_processing/rdBf.mat'],'rdBf_combined')
    save([INPUT '/post_processing/rP0f.mat'],'rP0f_combined')


end
