#!/bin/bash

if [ "$1" == -h ]
then

echo "t2s_combined_prep -o <output directory> -s <sample_name>"

exit

fi

while [ ! -z "$1" ]
do
case "$1" in
-o) od=${2};shift;;
-sn) sn=${2};shift;;
esac
#Prevent error using shift over 0 inputs
if [ $# -gt 0 ]
then
shift
fi

done

rm -rf ${od}/${sn}/post_processing_${sn}_t2s_reps_combined
cp -r ${od}/${sn}/post_processing_${sn}_rep1 ${od}/${sn}/post_processing_${sn}_t2s_reps_combined
rm ${od}/${sn}/post_processing_${sn}_t2s_reps_combined/niftidata_allTEs.nii
rm -rf ${od}/${sn}/post_processing_${sn}_t2s_reps_combined/T2s/
mkdir ${od}/${sn}/post_processing_${sn}_t2s_reps_combined/T2s/
mkdir ${od}/${sn}/post_processing_${sn}_t2s_reps_combined/T2s/logs
