function get_real_imag_onerep(path_to_2dseq_rep1, directory)

    
   
    system(['mkdir ' directory '/rep1']);
    system(['mkdir ' directory '/rep1/real']);
    system(['mkdir ' directory '/rep1/imag']);
    
    data_mouse = read_2dseq(path_to_2dseq_rep1);
    
    data_mouse = double(data_mouse);
    data_mouse_rep1 = squeeze(data_mouse);
   
    clear data_mouse; 
     
    real_dm_rep1=real(data_mouse_rep1);
    imag_dm_rep1=imag(data_mouse_rep1);
    
    
    
    for i=1:4
        nii=make_nii(real_dm_rep1(:,:,:,:,i), [0.1 0.1 0.1], [], 16);
        save_nii(nii,[directory '/rep1/real/real_rep1_ch' num2str(i) '.nii']);

        nii=make_nii(imag_dm_rep1(:,:,:,:,i), [0.1 0.1 0.1], [], 16);
        save_nii(nii,[directory '/rep1/imag/imag_rep1_ch' num2str(i) '.nii']);
         
    end 
    
 end
