function qsm_step1_gen(folder_allregistered, sample_name, no_TEs, no_coils, no_reps, voxelsize)
    
    
    
    for i=1:no_reps
        system(['mkdir -p ' folder_allregistered '/post_processing_' sample_name '_rep' num2str(i)]);
        system(['mkdir -p ' folder_allregistered '/post_processing_' sample_name '_rep' num2str(i) '/post_processing'])
        system(['mkdir -p ' folder_allregistered '/post_processing_' sample_name '_rep' num2str(i) '/logs'])
    end 
    
    for i=1:no_coils
        for j=1:no_reps
            data_mouse_real(:,:,:,:,i,j) = niftiread([folder_allregistered '/' sample_name '_complex_data/rep' num2str(j) '/real/real_rep' num2str(j) '_' num2str(i) '_resampled.nii.gz']);
            data_mouse_imag(:,:,:,:,i,j) = niftiread([folder_allregistered '/' sample_name '_complex_data/rep' num2str(j) '/imag/imag_rep' num2str(j) '_' num2str(i) '_resampled.nii.gz']);
        end
    end
    
    
    data_mouse=complex(data_mouse_real, data_mouse_imag);
    data_mouse=permute(data_mouse,[3 2 1 4 5 6]);
    data_mouse=flip(data_mouse,2);
    
    for l=1:no_reps
        for j=2:no_TEs
            for k=1:no_coils
            
    
        mag_data(:,:,:,j,k,l)=abs((data_mouse(:,:,:,j,k,l)));
        pha_data(:,:,:,j,k,l)=angle((data_mouse(:,:,:,j,k,l)));
    
            end
    
    pha_data_nooff(:,:,:,(j-1),:,l)=angle(exp(1i*pha_data(:,:,:,j,:,l))./exp(1i*(angle(data_mouse(:,:,:,1,:,l)))));

    mag_sum(:,:,:,(j-1),l)=sqrt(abs(sum((mag_data(:,:,:,j,:,l).^2).*exp(1i*pha_data_nooff(:,:,:,(j-1),:,l)),5)));

    phs_sum(:,:,:,(j-1),l)=angle(sum((mag_data(:,:,:,j,:,l).^2).*exp(1i*pha_data_nooff(:,:,:,(j-1),:,l)),5));

    end
    end
    
    temp=mag_sum(:,:,:,1,1);
   
    dim=size(temp);

    for k=1:dim(3)
    for j=1:dim(2)
        for ii=1:dim(1)

            if temp(ii,j,k)<3000
                temp(ii,j,k)=0;
            elseif temp(ii,j,k)>2999
                temp(ii,j,k)=1;
            end

        end
    end
    end


    for ii=1:dim(3)

        temp(:,:,ii)=~bwareaopen(~temp(:,:,ii),10);
        temp(:,:,ii)=bwareaopen(temp(:,:,ii),30);

    end


    for i=1:no_reps

        nii=make_nii(temp, [voxelsize voxelsize voxelsize], [], 64);
        save_nii(nii,[folder_allregistered '/post_processing_' sample_name '_rep' num2str(i) '/post_processing/mask.nii']);
        
        phasec=phs_sum(:,:,:,:,i);   
        save([folder_allregistered '/post_processing_' sample_name '_rep' num2str(i) '/post_processing/phasec.mat'],'phasec','-v7.3');

        cd1=angle(exp(1i*(phasec(:,:,:,2)-phasec(:,:,:,1))));
        phasec1=phasec(:,:,:,1);

        nii=make_nii(cd1, [voxelsize voxelsize voxelsize], [],64);
        save_nii(nii,[folder_allregistered '/post_processing_' sample_name  '_rep' num2str(i) '/post_processing/cd1.nii']);
       
        data_rep=mag_sum(:,:,:,:,i).*exp(1i*phs_sum(:,:,:,:,i));
        save([folder_allregistered '/post_processing_' sample_name '_rep' num2str(i) '/post_processing/combined_data.mat'],'data_rep','-v7.3');

        nii=make_nii(phasec1, [voxelsize voxelsize voxelsize], [],64);
        save_nii(nii,[folder_allregistered '/post_processing_' sample_name '_rep' num2str(i) '/post_processing/phasec1.nii']);
    
    end 
    
    
end

