function signal = signal_ev_MGE(M0, TE, R2s, noise)

    signal=(((M0*exp(-TE.*R2s)).^2+noise.^2)).^0.5;
end 