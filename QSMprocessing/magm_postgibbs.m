function magm_postgibbs(INPUT) 
    
    nii=load_nii([INPUT '/post_processing/magm_postcoilcomb_gibbs.nii']);
    magm=nii.img;
    
    save([INPUT '/post_processing/magm.mat'],'magm','-v7.3');
    
end
