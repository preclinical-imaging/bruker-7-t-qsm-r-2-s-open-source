#!/bin/sh

#  dBfitting.sh
#
#
#  Created by Chaoyue Wang on 05/07/2018.
#

while [ ! -z "$1" ]
do
case "$1" in
-i) input=${2};shift;;
-j) jobid=${2};shift;;
-no_TEs) no_TEs=${2};shift;;
-IES) IES=${2};shift;;
-first_TE) first_TE=${2};shift;;
esac
#Prevent error using shift over 0 inputs
if [ $# -gt 0 ]
then
shift
fi
done

# note this is hardcoded, it pre-specifies in how many chunks the code should be
# paralelised; can be changed if dataset is at very high resolution (<60um), but
# should run very fast even then
nslices=20

for k in `seq 1 ${nslices}`;
do
echo "matlab -singleCompThread -nodisplay -nosplash -r \"addpath('bruker-7-t-qsm-r-2-s-open-source/QSMprocessing');dBfitting_slice('$input', $k, $nslices, $no_TEs, $IES, $first_TE)\"" >> $input/logs/commands_dBfitting
done

jobid14=`fsl_sub -q long.q -l $input/logs/ -j $jobid -t $input/logs/commands_dBfitting`
echo $jobid14
