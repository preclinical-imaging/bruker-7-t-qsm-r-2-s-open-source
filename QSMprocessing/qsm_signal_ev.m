function signal=qsm_signal_ev(M0,phi0,freq,TE)
    
    signal=M0.*exp(1i*(phi0+2*pi*freq*TE*0.001));

end
