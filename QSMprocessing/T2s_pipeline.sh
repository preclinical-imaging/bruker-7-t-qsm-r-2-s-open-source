#!/bin/sh

#  T2s_pipeline.sh
#
#
#  Created by Chaoyue Wang on 18/01/2019.
#


# 0. read input and output

if [ "$1" == -h ]
then

echo "T2s -o <output directory> -r <voxel size> -i <registered_data_4D_nifti> \
-sn <sample_name> -mask <atlas or intensity-based thresholded brain mask in b0 space> \
-lf <location/storing folder for T2*/R2* maps> -repno <rep_number>"

exit

fi

while [ ! -z "$1" ]
do
case "$1" in
  -o) o=${2};shift;;
  -no_reps) no_reps=${2};shift;;
  -voxelsize) voxelsize=${2};shift;;
  -no_TEs) no_TEs=${2};shift;;
  -first_TE) first_TE=${2};shift;;
  -IES) IES=${2};shift;;
  -sn) sn=${2};shift;;
  -mask) mask=${2};shift;;
  -j) jobid=${2};shift;;

esac
#Prevent error using shift over 0 inputs
if [ $# -gt 0 ]
then
shift
fi

done


mkdir -p $o/${sn}/post_processing_${sn}_T2s/
mkdir -p $o/${sn}/post_processing_${sn}_T2s/logs
mkdir -p $o/${sn}/post_processing_${sn}_T2s/T2s

t2s_gibbs=${o}/$sn/${sn}_reg/*gibbs.nii.gz
jobid1=`fsl_sub -q veryshort.q -j $jobid -l $o/${sn}/post_processing_${sn}_T2s/logs \
bash bruker-7-t-qsm-r-2-s-open-source/QSMprocessing/T2s_fileprep.sh -i $t2s_gibbs -o $o/$sn/post_processing_${sn}_T2s/`

jobid2=`fsl_sub -q veryshort.q -j $jobid1 -l $o/${sn}/post_processing_${sn}_T2s/logs \
-s openmp,4 matlab -singleCompThread -nodisplay -nosplash \
-r "addpath('bruker-7-t-qsm-r-2-s-open-source/QSMprocessing');T2s_initial_estimates('$o/$sn/post_processing_${sn}_T2s/', $no_TEs, $IES, $first_TE)"`

jobid3=`bash bruker-7-t-qsm-r-2-s-open-source/QSMprocessing/T2sfitting.sh -i $o/${sn}/post_processing_${sn}_T2s/ -j $jobid2 -no_TEs $no_TEs -IES $IES -first_TE $first_TE`

nslices=20
jobid4=`fsl_sub -q veryshort.q -j $jobid3 -l $o/${sn}/post_processing_${sn}_T2s/logs \
-s openmp,4 matlab -singleCompThread -nodisplay \
-nosplash -r "addpath('bruker-7-t-qsm-r-2-s-open-source/QSMprocessing');fittingcomb_T2s('$o/$sn/post_processing_${sn}_T2s/T2s','${mask}',$voxelsize,$nslices)"`

jobid5=`fsl_sub -q veryshort.q -j $jobid4 -l $o/${sn}/post_processing_${sn}_T2s/logs bash bruker-7-t-qsm-r-2-s-open-source/QSMprocessing/T2_saveout.sh -o $o/$sn -sn $sn`

echo $jobid4
