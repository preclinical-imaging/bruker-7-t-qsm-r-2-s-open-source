function qsm_step1_reg_onerep(folder_allregistered, sample_name)


    system(['mkdir -p ' folder_allregistered '/post_processing_' sample_name '_rep1']);  
    system(['mkdir -p ' folder_allregistered '/post_processing_' sample_name '_rep1/post_processing'])

    
    real_dm_rep1_ch1_reg = niftiread([folder_allregistered '/' sample_name '_complex_data/rep1/real/real_rep1_1_resampled.nii.gz']);
    real_dm_rep1_ch2_reg = niftiread([folder_allregistered '/' sample_name '_complex_data/rep1/real/real_rep1_2_resampled.nii.gz']);
    real_dm_rep1_ch3_reg = niftiread([folder_allregistered '/' sample_name '_complex_data/rep1/real/real_rep1_3_resampled.nii.gz']);
    real_dm_rep1_ch4_reg = niftiread([folder_allregistered '/' sample_name '_complex_data/rep1/real/real_rep1_4_resampled.nii.gz']);
    
    imag_dm_rep1_ch1_reg = niftiread([folder_allregistered '/' sample_name '_complex_data/rep1/imag/imag_rep1_1_resampled.nii.gz']);
    imag_dm_rep1_ch2_reg = niftiread([folder_allregistered '/' sample_name '_complex_data/rep1/imag/imag_rep1_2_resampled.nii.gz']);
    imag_dm_rep1_ch3_reg = niftiread([folder_allregistered '/' sample_name '_complex_data/rep1/imag/imag_rep1_3_resampled.nii.gz']);
    imag_dm_rep1_ch4_reg = niftiread([folder_allregistered '/' sample_name '_complex_data/rep1/imag/imag_rep1_4_resampled.nii.gz']);
    
    data_mouse_rep1_real=zeros(size(real_dm_rep1_ch1_reg,1), size(real_dm_rep1_ch1_reg,2), size(real_dm_rep1_ch3_reg,3), size(real_dm_rep1_ch1_reg,4), 4);
    data_mouse_rep1_real(:,:,:,:,1) = real_dm_rep1_ch1_reg;
    data_mouse_rep1_real(:,:,:,:,2) = real_dm_rep1_ch2_reg;
    data_mouse_rep1_real(:,:,:,:,3) = real_dm_rep1_ch3_reg;
    data_mouse_rep1_real(:,:,:,:,4) = real_dm_rep1_ch4_reg;
    
    data_mouse_rep1_imag=zeros(size(real_dm_rep1_ch1_reg,1), size(real_dm_rep1_ch1_reg,2), size(real_dm_rep1_ch3_reg,3), size(real_dm_rep1_ch1_reg,4), 4);
    data_mouse_rep1_imag(:,:,:,:,1) = imag_dm_rep1_ch1_reg;
    data_mouse_rep1_imag(:,:,:,:,2) = imag_dm_rep1_ch2_reg;
    data_mouse_rep1_imag(:,:,:,:,3) = imag_dm_rep1_ch3_reg;
    data_mouse_rep1_imag(:,:,:,:,4) = imag_dm_rep1_ch4_reg;
    
    data_mouse_rep1=complex(data_mouse_rep1_real, data_mouse_rep1_imag);
    data_mouse_rep1=permute(data_mouse_rep1,[3 2 1 4 5]);
    data_mouse_rep1=flip(data_mouse_rep1,2);
    
    clear data_mouse_rep1_imag data_mouse_rep1_real imag_dm_rep1_ch1_reg imag_dm_rep1_ch2_reg imag_dm_rep1_ch3_reg imag_dm_rep1_ch4_reg real_dm_rep1_ch1_reg real_dm_rep1_ch2_reg real_dm_rep1_ch3_reg real_dm_rep1_ch4_reg
    
    
    % for all echo times 
    for j=2:20
    % for all coils
    for k=1:4
    
        mag_data_rep1(:,:,:,j,k)=abs((data_mouse_rep1(:,:,:,j,k)));
        pha_data_rep1(:,:,:,j,k)=angle((data_mouse_rep1(:,:,:,j,k)));
    
    end

    pha_data_nooff_rep1(:,:,:,(j-1),:)=angle(exp(1i*pha_data_rep1(:,:,:,j,:))./exp(1i*(angle(data_mouse_rep1(:,:,:,1,:)))));

    mag_sum_rep1(:,:,:,(j-1))=sqrt(abs(sum((mag_data_rep1(:,:,:,j,:).^2).*exp(1i*pha_data_nooff_rep1(:,:,:,(j-1),:)),5)));

    phs_sum_rep1(:,:,:,(j-1))=angle(sum((mag_data_rep1(:,:,:,j,:).^2).*exp(1i*pha_data_nooff_rep1(:,:,:,(j-1),:)),5));

    end

%     for j=2:20
%     % for all coils
%     for k=1:4
%     
%         mag_data_rep1(:,:,:,j,k)=abs((data_mouse_rep1(:,:,:,j,k)));
%         pha_data_rep1(:,:,:,j,k)=angle((data_mouse_rep1(:,:,:,j,k)));
%     end
% 
%     mag_sos_rep1(:,:,:,(j-1))=sqrt(abs(sum(mag_data_rep1(:,:,:,j,:).*conj(mag_data_rep1(:,:,:,j,:)),5)));
% 
%     end
%     
    clear data_mouse_rep1;
    
    data_rep1=mag_sum_rep1.*exp(1i*phs_sum_rep1);

    mag1_rep1=mag_sum_rep1(:,:,:,1);
    mag_rep1=mag_sum_rep1;
    
    %generate mask
    temp=mag1_rep1;
    dim=size(mag_rep1(:,:,:,1));

    for k=1:dim(3)
    for j=1:dim(2)
        for ii=1:dim(1)
            % Intensity threshold
            % May have to be adjusted for different sequences depending on
            % SNR of the image 
            if temp(ii,j,k)<3000
                temp(ii,j,k)=0;
            elseif temp(ii,j,k)>2999
                temp(ii,j,k)=1;
            end

        end
    end
    end


    for ii=1:dim(3)

        temp(:,:,ii)=~bwareaopen(~temp(:,:,ii),10);
        temp(:,:,ii)=bwareaopen(temp(:,:,ii),30);

    end

%smaller mask for eddy correction ROI
%     temped=mag_rep1(:,:,:,1);
%     for k=1:dim(3)
%         for j=1:dim(2)
%             for ii=1:dim(1)
% 
%                 if temped(ii,j,k)<8000
%                     temped(ii,j,k)=0;
%                 elseif temped(ii,j,k)>7999
%                     temped(ii,j,k)=1;
%                 end
%                 
%             end
%         end
%     end
%     for ii=1:dim(3)
% 
%         temped(:,:,ii)=bwareaopen(temped(:,:,ii),50);
% 
%     end

%     mask=temped;

    phasec=phs_sum_rep1;


    nii=make_nii(temp, [0.1 0.1 0.1], [], 64);
    save_nii(nii,[folder_allregistered '/post_processing_' sample_name '_rep1/post_processing/mask.nii']);

    save([folder_allregistered '/post_processing_' sample_name '_rep1/post_processing/phasec.mat'],'phasec','-v7.3');

    % changed from 4 - 2 on 11/02/2021
    cd1=angle(exp(1i*(phasec(:,:,:,2)-phasec(:,:,:,1))));
    phasec1=phasec(:,:,:,1);

    nii=make_nii(cd1, [0.1 0.1 0.1], [],64);
    save_nii(nii,[folder_allregistered '/post_processing_' sample_name  '_rep1/post_processing/cd1.nii']);

    save([folder_allregistered '/post_processing_' sample_name '_rep1/post_processing/combined_data.mat'],'data_rep1','-v7.3');

    nii=make_nii(phasec1, [0.1 0.1 0.1], [],64);
    save_nii(nii,[folder_allregistered '/post_processing_' sample_name '_rep1/post_processing/phasec1.nii']);

    clear cd1 data_rep1 mag1_rep1 mag_data_rep1 mag_rep1 mag_sos_rep1 mag_sum_rep1 mask pha_data_nooff_rep1 pha_data_rep1 phasec phasec1 phs_sum_rep1 temp temped;
    
    
    
end

