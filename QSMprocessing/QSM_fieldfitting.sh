#!/bin/sh

#  QSMP2.sh
#
#
#  Created by Chaoyue Wang on 30/10/2018.
#  Last edited by Cristiana Tisca on 18/01/2022

module add mrdegibbs3d

if [ "$1" == -h ]
then

echo "QSM_fieldfitting.sh -o <output_directory> -no_reps <number_of_repetitions>"
echo "-voxelsize <voxel_size> -no_TEs <number_of_echo_times>"
echo "-first_TE <first_echo_time> -IES <inter_echo_spacing> -j <jobid>"

exit

fi

while [ ! -z "$1" ]
do
case "$1" in
-o) o=${2};shift;;
-no_reps) no_reps=${2};shift;;
-voxelsize) voxelsize=${2};shift;;
-no_TEs) no_TEs=${2};shift;;
-first_TE) first_TE=${2};shift;;
-IES) IES=${2};shift;;
-j) jobid=${2};shift;;
esac
#Prevent error using shift over 0 inputs
if [ $# -gt 0 ]
then
shift
fi

done



for i in `seq 1 ${no_reps}`
do
qsm_r2s_postproc_folder=$( echo $o/post_processing*rep${i} )
echo "bruker-7-t-qsm-r-2-s-open-source/QSMprocessing/changebyte_fi.py $qsm_r2s_postproc_folder/post_processing/phasec1.nii 348 0" >> $qsm_r2s_postproc_folder/logs/pythonscript
echo "bruker-7-t-qsm-r-2-s-open-source/QSMprocessing/changebyte_fi.py $qsm_r2s_postproc_folder/post_processing/cd1.nii 348 0" >> $qsm_r2s_postproc_folder/logs/pythonscript
echo "bruker-7-t-qsm-r-2-s-open-source/QSMprocessing/changebyte_fi.py $qsm_r2s_postproc_folder/post_processing/mask.nii 348 0" >> $qsm_r2s_postproc_folder/logs/pythonscript

jobida1=`fsl_sub -q long.q -j $jobid -l $qsm_r2s_postproc_folder/logs -t $qsm_r2s_postproc_folder/logs/pythonscript`


jobid2=`bash bruker-7-t-qsm-r-2-s-open-source/QSMprocessing/deltaB.sh -i $qsm_r2s_postproc_folder -j $jobida1`


jobid3=`fsl_sub -q long.q -l $qsm_r2s_postproc_folder/logs \
-j $jobid2 matlab -singleCompThread -nodisplay -nosplash -r \
"addpath('bruker-7-t-qsm-r-2-s-open-source/QSMprocessing')\
;deltaB_fitting_gen('$qsm_r2s_postproc_folder', $no_TEs, $IES)"`


jobid4=`fsl_sub -q veryshort.q -l $qsm_r2s_postproc_folder/logs -j \
$jobid3 matlab -singleCompThread -nodisplay -nosplash -r \
"addpath('bruker-7-t-qsm-r-2-s-open-source/QSMprocessing');magm_pregibbs('$qsm_r2s_postproc_folder/',$voxelsize)"`


jobid5=`fsl_sub -q veryshort.q -j $jobid4 -l $qsm_r2s_postproc_folder/logs gzip $qsm_r2s_postproc_folder/post_processing/magm_postcoilcomb_pregibbs.nii`


jobid6=`fsl_sub -q short.q -j $jobid5 -l $qsm_r2s_postproc_folder/logs deGibbs3D $qsm_r2s_postproc_folder/post_processing/magm_postcoilcomb_pregibbs.nii.gz \
$qsm_r2s_postproc_folder/post_processing/magm_postcoilcomb_gibbs.nii.gz`


jobid7=`fsl_sub -q veryshort.q -j $jobid6 -l $qsm_r2s_postproc_folder/logs gunzip $qsm_r2s_postproc_folder/post_processing/magm_postcoilcomb_gibbs.nii.gz`


jobid8=`fsl_sub -q veryshort.q -l $qsm_r2s_postproc_folder/logs -j \
$jobid7 matlab -singleCompThread -nodisplay -nosplash -r \
"addpath('/bruker-7-t-qsm-r-2-s-open-source/QSMprocessing');magm_postgibbs('$qsm_r2s_postproc_folder/')"`


jobid9=`bash bruker-7-t-qsm-r-2-s-open-source/QSMprocessing/dBfitting.sh -i $qsm_r2s_postproc_folder -j $jobid8 -no_TEs $no_TEs -IES $IES -first_TE $first_TE`

# note this is hardcoded, it pre-specifies in how many chunks the code should be
# paralelised; can be changed if dataset is at very high resolution (<60um), but
# should run very fast even then
nslices=20

jobid=`fsl_sub -q long.q -l $qsm_r2s_postproc_folder/logs -j \
 $jobid9 matlab -singleCompThread -nodisplay -nosplash -r \
 "addpath('bruker-7-t-qsm-r-2-s-open-source/QSMprocessing');fittingcomb('$qsm_r2s_postproc_folder',$nslices)"`

done

echo $jobid
