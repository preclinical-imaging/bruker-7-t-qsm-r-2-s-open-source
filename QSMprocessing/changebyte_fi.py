#!/usr/bin/env fslpython
# usage: python changebyte.py filename.nii byteIndex value
#
# example: python changebyte.py filename.nii 348 0
import sys, shutil

filename = sys.argv[1]
offset = int(sys.argv[2], 0)
byte = int(sys.argv[3], 0)


with open(filename, "r+b") as fh:
    fh.seek(offset)
    fh.write(bytes([byte]))
