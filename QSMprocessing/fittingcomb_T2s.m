function fittingcomb_T2s(INPUT, brain_mask, vs, nslices)

    load([INPUT '/R2s_mask.mat'])

    mask_intensity_threshold=temp;

    mask_pipeline=niftiread(brain_mask);
    mask_pipeline=permute(mask_pipeline,[3 2 1]);
    mask_pipeline=flip(mask_pipeline,2);
    mask=mask_intensity_threshold.*mask_pipeline;

    dim=size(mask);

    R2sf_combined=zeros(dim);
    PDf_combined=zeros(dim);
    noisef_combined=zeros(dim);

    for i=1:nslices

        load([INPUT '/R2s_out' num2str(i) '.mat'])

        if i==1
            count_start=1;
        else 
            count_start=(round(dim(3)/20)*(i-1)+2);
        end 

        if i==nslices
            count_end=dim(3);
        else
            count_end=(round(dim(3)/20)*i+1);
        end

        for ii=count_start:count_end

            R2sf_combined(:,:,ii)=R2s(:,:,ii);
            PDf_combined(:,:,ii)=PD(:,:,ii);
            noisef_combined(:,:,ii)=noise(:,:,ii);
        end
    end 
    


    R2sf_combined=R2sf_combined.*mask;
    PDf_combined=PDf_combined.*mask;
    noisef_combined=noisef_combined.*mask;

    %save out data

    nii=make_nii(R2sf_combined, [vs vs vs], [16]);
    save_nii(nii,[INPUT '/R2sf.nii']);

    nii=make_nii(PDf_combined, [vs vs vs], [16]);
    save_nii(nii,[INPUT '/PDf.nii']);

    save([INPUT '/R2sf.mat'],'R2sf_combined')
    save([INPUT '/PDf.mat'],'PDf_combined')
    
    save([INPUT '/noisef.mat'],'noisef_combined')


end
