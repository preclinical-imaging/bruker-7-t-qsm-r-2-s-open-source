function magm_pregibbs(INPUT,voxelsize)
   
    magm=load([INPUT '/post_processing/magm.mat']);
    magm=cell2mat(struct2cell(magm));
   
    nii=make_nii(magm, [voxelsize voxelsize voxelsize], [], 16);
    save_nii(nii,[INPUT '/post_processing/magm_postcoilcomb_pregibbs.nii']);

end
