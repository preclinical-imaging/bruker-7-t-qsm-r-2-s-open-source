function T2s_initial_estimates(INPUT, no_TEs, IES, first_TE)

    mag=load_untouch_nii([INPUT '/niftidata_allTEs.nii']);
    mag=mag.img;
    mag=permute(mag,[3 2 1 4]);
    mag=flip(mag,2);

    dim=size(mag(:,:,:,1));

    temp=mag(:,:,:,2);

    for k=1:dim(3)
        for j=1:dim(2)
            for ii=1:dim(1)

                if temp(ii,j,k)<300
                    temp(ii,j,k)=0;
                elseif temp(ii,j,k)>299
                    temp(ii,j,k)=1;
                end

            end
        end
    end


    for ii=1:dim(3)

        temp(:,:,ii)=~bwareaopen(~temp(:,:,ii),10);
        temp(:,:,ii)=bwareaopen(temp(:,:,ii),50);

    end

    TE=first_TE:IES:((no_TEs)*IES);

    [M0,R2s] = R2sEst(mag(:,:,:,1:3),TE(1:3));

    M0m=M0.*temp;
    R2sm=R2s.*temp;

    for k=1:no_TEs
        magm(:,:,:,k)=mag(:,:,:,k).*temp;
    end

    system(['mkdir -p ' INPUT '/T2s'])

    save([INPUT '/T2s/M0est.mat'],'M0m');

    save([INPUT '/T2s/R2sest.mat'],'R2sm');

    save([INPUT '/T2s/Magm.mat'],'magm');

    save([INPUT '/T2s/R2s_mask.mat'],'temp');


end

