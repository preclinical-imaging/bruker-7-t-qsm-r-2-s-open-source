function signal = signal_ev_MGE_addnoise(M0, TE, R2s, noise)

    signal=M0*exp(-TE.*R2s)+noise;
end 