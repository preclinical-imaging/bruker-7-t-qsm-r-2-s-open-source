function signal=signal_ev_MGE_nonoise(M0,TE,R2s)

   signal=(M0*exp(-TE.*R2s));
   
end
