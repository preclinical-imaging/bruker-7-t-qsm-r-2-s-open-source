function check_complex_recon(complex_data_folder, output_folder)
    
    data_mouse = read_2dseq(complex_data_folder);
    
    if size(data_mouse)== [96   240   120    20     1     1     1     4]
        
        fileID = fopen([output_folder, '/data_check.txt'],'w');
        fprintf(fileID, '2dseq data raw dimensions are as expected.\n');
        fclose(fileID);
   
    else 
        
        fileID = fopen([output_folder, '/data_check.txt'],'w');
        fprintf(fileID, '2dseq data raw dimensions are not as expected.\n');
        fclose(fileID);
    end 
   
    
    data_mouse = double(data_mouse);
    data_mouse = squeeze(data_mouse);
    
    if isreal(data_mouse(50,50,50,1,1))
        
        fileID = fopen([output_folder, '/data_check.txt'],'a');
        fprintf(fileID, 'Data type is not complex, something has gone wrong. Complex data reconstruction was not done correctly and needs to be redone.');
        fclose(fileID);
   
    else 
        
        fileID = fopen([output_folder, '/data_check.txt'],'a');
        fprintf(fileID, 'Data type is complex, as expected.\n');
        fclose(fileID);
    end 
    
    real_dm=real(data_mouse);
    imag_dm=imag(data_mouse);
        

            
    nii=make_nii(real_dm(:,:,:,1,1), [0.1 0.1 0.1 ], [], 16);
    save_nii(nii,[output_folder '/real_vol1_ch1.nii']);
            
   
   
    

end
