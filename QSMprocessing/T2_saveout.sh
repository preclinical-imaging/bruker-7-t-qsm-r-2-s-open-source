#!bin/bash

#  T2s_fileprep.sh
#
#
#  Created by Cristiana Tisca on 18/03/2021.
#

while [ ! -z "$1" ]
do
case "$1" in
-sn) sn=${2};shift;;
-o) o=${2};shift;;
-repno) repno=${2};shift;;

esac
#Prevent error using shift over 0 inputs
if [ $# -gt 0 ]
then
shift
fi
done

cp ${o}/post_processing_${sn}_T2s/T2s/R2sf.nii ${o}/${sn}_t2s/${sn}_R2s.nii
gzip ${o}/${sn}_t2s/${sn}_R2s.nii
fslmaths ${o}/post_processing_${sn}_T2s/T2s/R2sf.nii -bin -div ${o}/post_processing_${sn}_T2s/T2s/R2sf.nii ${o}/${sn}_t2s/${sn}_T2s
