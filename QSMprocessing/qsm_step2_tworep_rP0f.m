function qsm_step2_tworep_rP0f(foldername_rep1, foldername_rep2, brain_mask, output_folder, sample_name, additional_args)
   
    
    load([foldername_rep1 '/post_processing/rdBf.mat'])
    load([foldername_rep1 '/post_processing/rP0f.mat'])
    
    rdBf1=rdBf_combined;
    rP0f=rP0f_combined;
    
    load([foldername_rep2 '/post_processing/rdBf.mat'])
    
    rdBf2=rdBf_combined;
    

    mask_intensity_threshold=niftiread([foldername_rep1 '/post_processing/mask.nii']);
    mask_pipeline=niftiread(brain_mask);
    mask_pipeline=permute(mask_pipeline,[3 2 1]);
    mask_pipeline=flip(mask_pipeline,2);
    mask=mask_intensity_threshold.*mask_pipeline;
    
    rdBf=(rdBf1+rdBf2)/2;
    rdBft=rdBf*2*pi;
    
    dim=size(rdBf);

    for ii=1:dim(3)

        mask(:,:,ii)=bwareaopen(mask(:,:,ii),20);
        mask(:,:,ii)=~bwareaopen(~mask(:,:,ii),20);

    end


    %%your voxelsize here
    vx=0.1;

    vy=0.1;

    vz=0.1;
    
    [dB_vsf,mask_vsf]=V_SHARP(rdBft,mask,'voxelsize', [1 1 1], 'smvsize', 20);
 
    mask_vsf(rP0f>1)=0;
    mask_vsf(rP0f<-1)=0;
    
    niftiwrite(rdBf, [output_folder '/' sample_name '_rdBf'  additional_args])
    niftiwrite(rP0f, [output_folder '/' sample_name '_rP0f' additional_args])
    niftiwrite(dB_vsf, [output_folder '/' sample_name '_dB_vsf'  additional_args])
    
    B0=7;
    TE=1;
    H=[0 1 0];


    [susceptibility]= QSM_iLSQR(dB_vsf,mask_vsf,'TE',TE,'B0',B0,'H',H,'padsize',[6,6,6],'voxelsize',[vx vy vz]);
    nii=make_nii(susceptibility, [0.1 0.1 0.1], [],64);
    save_nii(nii,[output_folder '/' sample_name '_susceptibility' additional_args '.nii']);
    
end

