#!/bin/sh

#  T2s_fileprep.sh
#  
#
#  Created by Cristiana Tisca on 18/03/2021.
#  

while [ ! -z "$1" ]
do
case "$1" in
-i) input=${2};shift;;
-o) output=${2};shift;;
esac
#Prevent error using shift over 0 inputs
if [ $# -gt 0 ]
then
shift
fi
done

cp $input $output/niftidata_allTEs.nii.gz
gunzip $output/niftidata_allTEs.nii.gz 

