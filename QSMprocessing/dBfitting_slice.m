function dBfitting_slice(INPUT, slice, nslices, no_TEs, IES, first_TE)
   
    phasecm=load([INPUT '/post_processing/phasecm.mat']);

    phasecm=cell2mat(struct2cell(phasecm));

    magm=load([INPUT '/post_processing/magm.mat']);

    magm=cell2mat(struct2cell(magm));

    dBm=load([INPUT '/post_processing/dBm.mat']);

    dBm=cell2mat(struct2cell(dBm));

    phi0m=load([INPUT '/post_processing/phi0m.mat']);

    phi0m=cell2mat(struct2cell(phi0m));

    nii=load_nii([INPUT '/post_processing/mask.nii']);
    mask=nii.img;


    phasecm=double(phasecm);
    magm=double(magm);
    dBm=double(dBm);
    phi0m=double(phi0m);
    mask=double(mask);

    dim=size(dBm);

    rP0f=zeros(dim);
    rdBf=zeros(dim);

    if slice==1
        count_start=1;
    else 
        count_start=(round(dim(3)/20)*(slice-1)+2);
    end 

    if slice==nslices
        count_end=dim(3);
    else
        count_end=(round(dim(3)/20)*slice+1);
    end 

    t=[1:(no_TEs-1)].';
    TE=first_TE:IES:((no_TEs-1)*IES);

    for j=count_start:count_end
        for k=1:dim(1)
            for l=1:dim(2)

                if mask(k,l,j)==1

                    y=squeeze([(magm(k,l,j,:).^2.*exp(1i*phasecm(k,l,j,:)))]).';

                    M=squeeze([magm(k,l,j,:).^2]).';

                    fun=@(A)[real(qsm_signal_ev(M,A(1),A(2),TE)-y(t)), imag(qsm_signal_ev(M,A(1),A(2),TE)-y(t))];

                    x0(1)=phi0m(k,l,j);
                    x0(2)=dBm(k,l,j);

                    options = optimoptions('lsqnonlin','Display','off');

                    x=lsqnonlin(fun,x0,[],[],options);

                    rP0f(k,l,j)=x(1);
                    rdBf(k,l,j)=x(2);

                end
            end

        end
    end

    save([INPUT '/post_processing/fitting_out' num2str(slice) '.mat'], 'rdBf','rP0f')

end
