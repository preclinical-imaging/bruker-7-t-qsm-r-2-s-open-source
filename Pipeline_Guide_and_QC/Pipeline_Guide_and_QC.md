# Guide to quality control and post-processing of the ex vivo GRE mouse brain data 
### Last reviewed: 27.06.2023


>If you have any questions, need help or further information/clarification, please contact me at cristiana.tisca@yahoo.com.

---
### Before you run the pipeline:
You need to have brain masks ready before you run the pipeline. The final QSM outputs are going to be affected by the presence of non-brain voxels, particularly bubbles. This preliminary step should be performed in order to ensure that your maps are of good quality. 

You can use the first echo (or a later echo, for increased contrast - but make sure that it is registered to the first echo) of the first repetition for extracting a brain mask (using e.g. the MBM.py pipeline), which you can feed into the MBM.py pipeline. You may use consecutive dilation/erosion steps on the brain mask (using `fslmaths` or `minctools`, for example) to ensure that the brain volume is covered appropriately.


---
### Running the QSM and R2*/T2* pipelines 
* Step 0: Creating a configuration file. 
As a first step before running this pipeline, you can create a configuration file. This is optional, as the pipeline also runs if you input the required parameters in the command line instead. You can run the pipeline for the first 1-2 samples using the configuration file to make sure the pipeline is working well with the given inputs before running it on the whole cohort. An empty configuration file which you can copy down and complete can be found in the bin directory `bruker-7-t-qsm-r-2-s-open-source/bin/config.cfg`. The needed parameters can be filled in for each individual sample. An example completed configuration file can be found below (the scan-specific parameters are the default ones for the ex vivo acquisition on the Bruker 7T at the BSB). You can create your own and name it any way you like (e.g. sample_name_config.cfg) and place it in a directory of config files. 

To run the pipeline for the standard WIN ex vivo protocol (using both repetitions), execute the following commands:

* Step 1: Prepare your data - this script registers the magnitude and complex data (ensuring that all volumes are well-aligned) and performs Gibbs-ringing correction. It also creates a masterfiles folder in the sample directory, where the scan-specific information (first echo time, inter-echo spacing, number of channels, number of repetitions, number of echo times, sample name, voxel size) is stored in text files. 

Note that a folder bearing your sample name will be created in your specified output directory. This is where all the data processing will take place. 

There are two ways of running this script: you can either create a configuration file and run the following command:  
`bruker-7-t-qsm-r-2-s-open-source/prep_data_qsm_r2s.sh –-config_file <config_file_name> `
 
Alternatively, you can input the scan-specific information directly into the command line, without the need of creating a configuration file. 

`bruker-7-t-qsm-r-2-s-open-source/prep_data_qsm_r2s.sh –-o <output_directory> –-sn <sample_name> --no_reps <number of repetitions> --no_channels <number of channels> --no_TEs <number of echo times> --IES <inter-echo spacing> --voxelsize <voxelsize> --magnitude_data_1 <folder with nifti file for the first repetition> –-magnitude_data_2 <folder with nifti file for the second repetition> –-complex_data_1 <folder with 2dseq file for the first repetition> --complex_data_2 <folder with 2dseq file for the second repetition> –-mask <mask from the atlas-based segmentation> –-first_TE <first_TE> `
* Step 2: Run the QSM/R2* pipeline once the previous step has finished. This runs the following steps: For QSM - coil combination, field map estimation, background field removal and dipole inversion. For T2s/R2s pipeline: fitting the Gibbs-corrected magnitude data. Finally, the header output data is corrected so that the data can be displayed in anatomical orientation. By default, the intermediary files created in the pipeline are cleared up once the files of interest are generated to avoid running into memory issues (before cleaning up, the directory usually has 10GB). After cleanup, only the files of interest are kept (susceptibility, R2s and T2s maps), reducing the folder size to 5MB. To run it, once the previous step has finished, execute the following command: 

`bruker-7-t-qsm-r-2-s-open-source/qsm_pipeline_data_prepped.sh -o <output_directory> -sn <sample_name> -mask <brain_mask>`

Alternatively, if you want to keep all intermediary files (which is very useful for quality control or debugging purposes), you should run this command. This is recommended for initial runs to make sure everything is running properly and there are no errors. It also helps for debugging as all log files are kept. 

`bruker-7-t-qsm-r-2-s-open-source/qsm_pipeline_data_prepped.sh -o <output_directory> -sn <sample_name> -mask <brain_mask> –-no_cleanup `

Two different coil combination methods are implemented within this pipeline. The default one was used in the ex vivo pipeline that this pipeline is based on (Wang et al, 2020, NeuroImage,  doi: 10.1016/j.neuroimage.2020.117216). An alternative method is implemented within the pipeline and can be used if the data quality is not good to improve the appearance of the susceptibility maps (Eckstein et al, 2018, doi: 10.1002/mrm.26963), however that should not make a difference for the ex vivo data as this is quite high quality/SNR. 

If you want to try the alternative coil combination method, you can run this command: 

`bruker-7-t-qsm-r-2-s-open-source/qsm_pipeline_data_prepped.sh -o <output_directory> -sn <sample_name> -mask <mask_file> –-MCPC_3D`  

---
### Output files and folders
If you ran the default version of the pipeline, then only the files of interest are kept in your output folder. The output folder will have the name of your sample and the following folders and files: 

`qsm_r2s_parametric_maps`: This is the folder containing your files of interest. These are: 
* `susceptibility.nii.gz`: The quantitative susceptibility map. 
* `T2s.nii.gz`: The T2* map. 
* `R2s.nii.gz`: The R2* map. 
You can choose either T2s or R2s for the analysis, they are each other’s inverse. 

`logs`: This folder contains two files, listing all commands that were run. 
* `commands_prep_data_qsm_r2s`: the commands run in the first stage of the pipeline 
* `commands_data_prepped_qsm_r2s`: commands run in the second stage of the pipeline. 

`masterfiles`: The parameters used in running the pipeline are stored in text files in this folder. This can be used as a retrospective check-up. 
* `first_TE`: first echo time 
* `IES`: inter-echo spacing 
* `no_channels`: number of channels 
* `no_reps`: number of repetitions 
* `no_TEs`: number of echo times 
* `sample_name`: sample_name 
* `voxelsize`: voxel size 


---
Alternatively, if you’ve run the `--no_cleanup` flag, your output folder will contain the following folders and files:

`<sample_name>_reg` - Contains the follwing folders and files:
* `gibbslogs`: log files from Gibbs correction 
* `transforms_rep1(2)`: folder with `.mat` transforms from `FLIRT` which will be used for registration of the complex data and were outputted from the magnitude data registration 
* `<sample_name>_magnitude_data_raw_rep1(2).nii.gz`: local copies of the magnitude files 
* `<sample_name>_rep1(2).nii.gz`: magnitude files after registration 
* `<sample_name>_avg.nii.gz`: average of the magnitude files after registration 
* `<sample_name>_avg_gibbs.nii.gz`: gibbs-corrected magnitude file (after registration and averaging across repetitions), used for T2*/R2* mapping 


`<sample_name>_complex_data`- Contains the following folders:
	
* `rep1/2`: each contain folders real and imag, where you can find a 4D nifti file (20 volumes or equivalent volumes depending on the number of echo times) per individual coil, before and after resampling using the transformes derived from the registration (4 files in total pre resampling, e.g. real_rep1_ch1.nii, real_rep1_ch2.nii, real_rep1_ch3.nii, real_rep1_ch4.nii and four after resampling, real_rep1_ch1_resampled.nii.gz, real_rep1_ch2_resampled.nii.gz, real_rep1_ch3_resampled.nii.gz, real_rep1_ch4_resampled.nii.gz) 
* `complex_data_rep1/2`: local copies of the complex data  

`<sample_data>_qsm` - Contains the following files:
* `<sample_name>_rdBf.nii`: estimated field map 
* `<sample_name>_dB_vsf.nii`: estimated field map after background field removal 
* `<sample_name>_rP0f.nii`: estimated phase offset map 
* `<sample_name>_susceptibility.nii.gz`: estimated field map – susceptibility maps in correct anatomical orientation 

`<sample_name>_t2s` - Contains the following files: 
* `<sample_name>_R2sf.nii.gz`: R2s maps in correct anatomical orientation 
* `<sample_name>_T2s_nii.gz`: T2s maps in correct anatomical orientation 

`post_processing_<sample_name>_rep1`: Intermediary folders and log files for susceptibility mapping (rep1) 

`post_processing_<sample>_rep2`: Intermediary folders and log files for susceptibility mapping (rep2) 

`post_processing_<sample_name>_reps_combined`: Intermediary folders and log files for T2*/R2* mapping (reps combined) 

`logs`: Log files various stages of the pipeline. Of interest should be the following files: 
* `commands_prep_data_qsm_r2s`: contains the commands run in the first stage of the pipeline 
* `commands_data_prepped_qsm_r2s`: contains the commands run in the second stage of the pipeline. 
`masterfiles`: The parameters used in running the pipeline are stored in text files in this folder. This can be used as a retrospective check. 
* `first_TE`: first echo time 
* `IES`: inter-echo spacing 
* `no_channels`: number of channels 
* `no_reps`: number of repetitions 
* `no_TEs`: number of echo times 
* `sample_name`: sample_name 
* `voxelsize`: voxel size 

---
### Referencing the QSM data 

Note on QSM maps: the susceptibility values obtained via the pipeline will need to be referenced. In our study, we lack a reliable reference region. Therefore, susceptibility values should be referenced to the whole brain by setting the mean susceptibility of the whole brain to zero. Therefore, for the QSM map, subtract the mean of susceptibility values within the brain mask from each voxel in the brain mask.  

There is a script for that in the pipeline repository. 

`bruker-7-t-qsm-r-2-s-open-source/reference_QSM_output.sh <input_directory> <sample_name> <brain_mask>`

---
### Notes:
* The pipeline outputs susceptibility maps and T2*/R2*. These maps are obtained using both repetitions, if they are supplied - this should be the case for the ex vivo WIN 7T  MGE data. Otherwise, the pipeline runs only for one repetition.  

* The final susceptibility and T2*/R2* maps have been reoriented into correct anatomical orientation, so ROI-based analysis in native space can be performed on these maps. To transform maps back into standard space for standard-space analysis, the .omat files need to be converted to .xfm and then concatenated with the .xfm files from the MBM.py registration pipeline.

---
### Basic Quality Control

Check all above files and directory are present in the directory you created. Check 4D file sizes are as expected.
Inspect the outputs listed above in FSLeyes, ensuring that each processing step was successfully completed and that no major artifacts are persent.

**Images to QC**:

* Example susceptibility map (FSLeyes windowing -200 to +200): 
	
Ensure that the grey-white matter contrast is as expected (with low susceptibility values attributed to white matter) and that the susceptibility values are roughly uniform across the cortex. Pay attention to the edge of the cortex, ensuring that there are no major outliers that might be influencing results.

<img src="Photos/susceptibility.png" alt="Figure 1. Example susceptibility map."
	title="Figure 1. Example susceptibility map." width="500" height="200" />

* Example T2s map:

Ensure that the grey-white matter contrast is as expected and that the T2s values are sensible. 

<img src="Photos/T2smap.png" alt="Figure 2. Example T2* map."
	title="Figure 2. Example T2* map." width="500" height="200" />

* Example magnitude data (echo 6):

QC the data following registration, for each individual repetition. Ensure that the signal is homogeneous across the cortex, and that the grey-white matter contrast is as expected. Evaluate the signal decay profile in a few voxels in `FSLeyes` to ensure that it is roughly exponential. Evaluate the output after Gibbs ringing correction to ensure that ringing present in any of the three dimensions was corrected. 

<img src="Photos/magnitude.png" alt="Figure 3. Example magnitude image."
	title="Figure 3. Example magnitude image." width="500" height="200" />

* Example estimated field map:

We expect the field map to be roughly uniform across the brain. Any salt-and-pepper-like noise on the images may indicate poor fitting, which will need to be investigated. 

<img src="Photos/field_map.png" alt="Figure 4. Example field map."
	title="Figure 4. Example field map." width="500" height="200" />

* Example estimated phase offset map:

We expect very small variations across the cortex, with the largest variations occuring at the edge, where air microbubbles are present. 

<img src="Photos/phase_offset_map.png" alt="Figure 5. Example phase offset map."
	title="Figure 5. Example phase offset map." width="500" height="200" />

* Example estimated field map after background field removal (FSLeyes windowing -200 to +200):

We expect good grey-white matter contrast, similar to the susceptibility maps. 

<img src="Photos/field_map_after_background_field_removal.png" alt="Figure 6. Example field map after background field removal."
	title="Figure 6. Example field map after background field removal." width="500" height="200" />


 

